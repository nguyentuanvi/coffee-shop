package com.strapflatapp.zalosdk;

import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.zing.zalo.zalosdk.oauth.LoginVia;
import com.zing.zalo.zalosdk.oauth.OAuthCompleteListener;
import com.zing.zalo.zalosdk.oauth.OauthResponse;
import com.zing.zalo.zalosdk.oauth.ZaloSDK;

/**
 * Created by new_user on 7/9/18.
 */

public class ZaloSDKModule extends ReactContextBaseJavaModule {
    private final static String TAG = "Zalo-SDK";

    public ZaloSDKModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "Zalo";
    }

    @ReactMethod
    public void login(final Promise promise) {
        ZaloSDK.Instance.authenticate(this.getCurrentActivity(), LoginVia.APP_OR_WEB, new OAuthCompleteListener(){
            @Override
            public void onAuthenError(int errorCode, String message) {
                Log.e(TAG, "Error code: " + String.valueOf(errorCode));

//                WritableMap map = Arguments.createMap();
//                map.putInt("error_code", errorCode);
//                map.putString("message", message);

                promise.reject(String.valueOf(errorCode), message);
            }
            @Override
            public void onGetOAuthComplete(OauthResponse response) {
                Log.i(TAG, "Zalo uid: " + String.valueOf(response.getuId()));
                Log.i(TAG, "Zalo oauth code: " + response.getOauthCode());
                Log.i(TAG, "Zalo channel: " + response.getChannel().toString());

                WritableMap map = Arguments.createMap();
                map.putString("uid", String.valueOf(response.getuId()));
                map.putString("oauth_code", response.getOauthCode());
                map.putString("channel", response.getChannel().toString());
                map.putBoolean("is_register", response.isRegister());

                promise.resolve(map);
            }
        });
    }
}
