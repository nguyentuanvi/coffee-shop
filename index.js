import { AppRegistry } from 'react-native';
import App from './App';
import bgMessageHandler from './src/boot/bgMessaging';

AppRegistry.registerComponent('StrapFlatApp', () => App);
// New task registration
AppRegistry.registerHeadlessTask(
  'RNFirebaseBackgroundMessage',
  () => bgMessageHandler
); // <-- Add this line
