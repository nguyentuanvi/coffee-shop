import React, { Component } from 'react';
import { SafeAreaView, AsyncStorage } from 'react-native';
import { Provider } from 'react-redux';
import { isIphoneX } from 'react-native-iphone-x-helper';
import firebase from 'react-native-firebase';
import SplashScreen from 'react-native-splash-screen';

import App from '../App';
import configureStore from './configureStore';
import { Logger, Consts, Storage } from '../common';

export default class Setup extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      store: configureStore(() => this.setState({ isLoading: false }))
    };

    firebase.initializeApp();

    console.disableYellowBox = true;
  }

  requestNotificationPermission() {
    firebase
      .messaging()
      .requestPermission()
      .then(() => {
        // User has authorised
        Logger.log('Notification authorized');
      })
      .catch(error => {
        // User has rejected permissions
        Logger.log(error);
      });
  }

  getDeviceToken() {
    firebase
      .messaging()
      .getToken()
      .then(fcmToken => {
        if (fcmToken) {
          // user has a device token
          Logger.log('Device token :', fcmToken);
          AsyncStorage.setItem(Consts.DEVICE_TOKEN, fcmToken);
        } else {
          // user doesn't have a device token yet
        }
      });
  }

  initMessageListener() {
    if (this.messageListener) return;

    this.messageListener = firebase.messaging().onMessage(message => {
      // Process your message as required
      Logger.log('Notification messaging: ', message);
    });
  }

  initialNotification() {
    firebase
      .notifications()
      .getInitialNotification()
      .then(notificationOpen => {
        if (notificationOpen) {
          // App was opened by a notification
          // Get the action triggered by the notification being opened
          const action = notificationOpen.action;
          // Get information about the notification that was opened
          const notification = notificationOpen.notification;

          Logger.log('Init notification: ', notificationOpen);
        }
      });
  }

  initNotificationDisplayedListener() {
    this.notificationDisplayedListener = firebase
      .notifications()
      .onNotificationDisplayed(notification => {
        Logger.log('Notification displayed: ', notification);
        // Process your notification as required
        // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
      });
  }

  initNotificationReceiveListener(channel) {
    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => {
        // Process your notification as required
        Logger.log('Notification: ', notification);

        const _notification = new firebase.notifications.Notification()
          .setNotificationId('NotificationId')
          .setTitle(notification.title)
          .setBody(notification.body);

        _notification.android
          .setChannelId(channel.channelId)
          .android.setSmallIcon('ic_launcher');

        // Display the notification
        firebase.notifications().displayNotification(_notification);
      });
  }

  initNotificationOpenedListener() {
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        // Get the action triggered by the notification being opened
        const action = notificationOpen.action;
        // Get information about the notification that was opened
        const notification = notificationOpen.notification;

        Logger.log('Notification open: ', notificationOpen);
      });
  }

  async componentDidMount() {
    // Build a channel
    const channel = new firebase.notifications.Android.Channel(
      'test-channel',
      'Test Channel',
      firebase.notifications.Android.Importance.Max
    ).setDescription('My apps test channel');

    // Create the channel
    firebase.notifications().android.createChannel(channel);

    this.requestNotificationPermission();

    this.getDeviceToken();

    this.initMessageListener();

    this.initialNotification();

    this.initNotificationDisplayedListener();

    this.initNotificationReceiveListener(channel);

    this.initNotificationOpenedListener();

    SplashScreen.hide();
  }

  componentWillUnmount() {
    this.messageListener();
    this.notificationDisplayedListener();
    this.notificationListener();
    this.notificationOpenedListener();
  }

  render() {
    const UsingSafeAreaView = isIphoneX() ? (
      <SafeAreaView>
        <App />
      </SafeAreaView>
    ) : (
      <App />
    );

    return <Provider store={this.state.store}>{UsingSafeAreaView}</Provider>;
  }
}
