// @flow
import { AsyncStorage } from 'react-native';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from 'redux-saga';

import reducers from '../reducers';
import rootSagas from '../services/sagas/rootSagas';

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducers);

export default function configureStore(onCompletion: () => void): any {
  // create the saga middleware
  const sagaMiddleware = createSagaMiddleware();

  const enhancer = compose(applyMiddleware(thunk, logger, sagaMiddleware));

  const store = createStore(persistedReducer, enhancer);
  persistStore(store, onCompletion);

  // run saga
  sagaMiddleware.run(rootSagas);

  return store;
}
