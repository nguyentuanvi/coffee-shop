import React, { Component } from 'react';
import { View, ActivityIndicator, Text } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { AppStyles, Colors } from './index';

const styles = EStyleSheet.create({
  // $outline: 1,
  footer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '$padding_layout'
  },
  loading: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '$padding_content'
  }
});

/**
 * render empty list
 */
export function renderEmptyList(text, showIndicator = true) {
  return (
    <View style={styles.loading}>
      {showIndicator && <ActivityIndicator animating size="large" color={Colors.COLOR_YELLOW} />}
      <Text style={[AppStyles.fontContent, AppStyles.marginLayoutLeft, AppStyles.fontColorLabel]}>
        {text}
      </Text>
    </View>
  );
}

/**
 * render separator item
 */
export function renderSeparatorItem(height = 1, backgroundColor = Colors.COLOR_LIGHT_GRAY) {
  return <View style={{ width: '100%', height: height, backgroundColor: backgroundColor }} />;
}
