import * as Utils from './utils';

export const required = value =>
  Utils.isNullOrUndefined(value) ? 'Required' : undefined;

export const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined;

export const maxLength15 = maxLength(15);

export const minLength = min => value =>
  value && value.length < min ? `Must be ${min} characters or more` : undefined;

export const minLength8 = minLength(8);

export const email = value =>
  !Utils.validateEmail(value) ? 'Invalid email address' : undefined;

export const alphaNumeric = value =>
  Utils.isAlphaOrNumberic(value) ? 'Only alphanumeric characters' : undefined;

export const phoneNumber = phoneNo => {
  let regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
  return regex.test(phoneNo) ? undefined : 'Invalid phone number';
};
