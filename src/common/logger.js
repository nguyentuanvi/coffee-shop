export default class Logger {
  static log(message, ...args) {
    if (__DEV__) {
      console.log(message, ...args);
    }
  }

  static debug(message, ...args) {
    if (__DEV__) {
      console.log('[DEBUG] ' + message, ...args);
    }
  }

  static info(message, ...args) {
    console.log('[INFO] ' + message, ...args);
  }

  static error(message, ...args) {
    console.log('[ERROR] ' + message, ...args);
  }
}
