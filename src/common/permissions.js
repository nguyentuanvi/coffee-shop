import Permissions from 'react-native-permissions';

import Logger from './logger';
/*
name     type     ios android
Location	location	✔️	✔
Camera	camera	✔️	✔
Microphone	microphone	✔️	✔
Photos	photo	✔️	✔
Contacts	contacts	✔️	✔
Events	event	✔️	✔
Bluetooth	bluetooth	✔️	❌
Reminders	reminder	✔️	❌
Push Notifications	notification	✔️	❌
Background Refresh	backgroundRefresh	✔️	❌
Speech Recognition	speechRecognition	✔️	❌
mediaLibrary	mediaLibrary	✔️	❌
Motion Activity	motion	✔️	❌
Storage	storage	❌️	✔
Phone Call	callPhone	❌️	✔
Read SMS	readSms	❌️	✔
Receive SMS	receiveSms	❌️	✔
*/

export const PERMISSIONS = {
  Location: 'location',
  Camera: 'camera',
  Microphone: 'microphone',
  Photos: 'photo',
  Contacts: 'contacts',
  PhoneCall: 'callPhone',
  ReadSMS: 'readSms',
  ReceiveSMS: 'receiveSms'
};

export default class PermissionUtils {
  static isAuthorized(resp) {
    return resp === 'authorized';
  }

  static check(type) {
    return new Promise((resolve, reject) => {
      Permissions.check(type)
        .then(response => {
          // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
          resolve(PermissionUtils.isAuthorized(response));
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  static request(type) {
    return new Promise((resolve, reject) => {
      Permissions.request(type)
        .then(response => {
          // Returns once the user has chosen to 'allow' or to 'not allow' access
          // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
          resolve(PermissionUtils.isAuthorized(response));
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  static checkAndRequest(type) {
    return new Promise((resolve, reject) => {
      PermissionUtils.check(type)
        .then(response => {          
          if (response !== 'authorized') {
            PermissionUtils.request(type)
              .then(resp => {
                resolve(resp);
              })
              .catch(err => {
                Logger.log(err);
                reject(err);
              });
          } else {
            resolve(true);
          }
        })
        .catch(err => {
          Logger.log(err);
          reject(err);
        });
    });
  }
}
