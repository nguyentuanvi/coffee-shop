import { Platform, ToastAndroid } from 'react-native';
import { Toast } from 'native-base';
import RNFetchBlob from 'react-native-fetch-blob';
import RNFS from 'react-native-fs';
import ImagePicker from 'react-native-image-picker';
import Snackbar from 'react-native-snackbar';
import { Logger } from '.';

/**
 * @param {*} param
 */
export function isNullOrUndefined(param) {
  return param === null ||
    param === undefined ||
    param === '' ||
    param.length <= 0
    ? true
    : false;
}

export function isEmptyString(param) {
  return param === null || param === undefined || param.trim().length <= 0
    ? true
    : false;
}

/**
 * @param {*} param
 */
export function isNullOrEmptyItems(param) {
  return param === null || param === undefined || param.length <= 0
    ? true
    : false;
}

export function validateEmail(value) {
  return value && /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value);
}

export function isAlphaOrNumberic(value) {
  return value && /[^a-zA-Z0-9 ]/i.test(value);
}

export function isFunction(func) {
  return func && typeof func === 'function';
}

export function showAlert(
  msg,
  duration = 3000,
  position = 'bottom',
  type = ''
) {
  if (Platform.OS === 'android') {
    ToastAndroid.show(msg, ToastAndroid.SHORT);
  } else {
    Toast.show({
      text: msg,
      duration: duration,
      position: position,
      textStyle: { textAlign: 'center', color: 'white' },
      type: type
    });
  }
}

export function showSuccessAlert(msg, duration = 3000, position = 'bottom') {
  showAlert(msg, duration, position, 'success');
}

export function showErrorAlert(msg, duration = 3000, position = 'bottom') {
  showAlert(msg, duration, position, 'danger');
}

export function showSnackbar(title, duration = Snackbar.LENGTH_LONG, action) {
  Snackbar.show({
    title,
    duration,
    action
  });
}

export function downloadFile(
  url,
  mime,
  filePath,
  onProgress,
  onSuccess,
  onFailed
) {
  RNFetchBlob.config({
    fileCache: true,
    // DCIMDir is in external storage
    path: filePath
  })
    .fetch('GET', url)
    .progress((received, total) => {
      console.log('Download progress: ', received / total);
      if (onProgress) {
        onProgress(received, total);
      }
    })
    .then(res => {
      console.log(res);
      if (Platform.OS === 'android') {
        RNFetchBlob.fs
          .scanFile([{ path: res.path(), mime: mime }])
          .then(() => {
            console.log('Scan file successfully: ');
          })
          .catch(err => {
            console.log(err);
          });
      }

      if (onSuccess) {
        onSuccess(res.path());
      }
    })
    .then(() => {
      // scan file success
    })
    .catch(err => {
      // scan file error
      console.log(err);
      if (onFailed) {
        onFailed(err);
      }
    });
}

/**
 * Check file existence with filename
 * @param {*} filePath
 * @param {*} onSuccess
 * @param {*} onError
 */
export function checkFileExist(filePath, onSuccess, onError) {
  RNFS.exists(filePath)
    .then(exists => {
      if (onSuccess && typeof onSuccess === 'function') {
        onSuccess(exists);
      }
    })
    .catch(err => {
      console.warn(err);
      if (onError && typeof onError === 'function') {
        onError(err);
      }
    });
}

export function selectPhoto(callback = () => {}, options = {}) {
  let defaultOptions = {
    title: 'Select image',
    noData: true,
    quality: 1.0,
    storageOptions: {
      skipBackup: true
    }
  };

  let _options = Object.assign({}, defaultOptions, options);

  ImagePicker.showImagePicker(_options, response => {
    Logger.log('Response = ', response);

    let success = false;
    let errorMsg = null;
    let result = { ...response };

    if (response.didCancel) {
      Logger.log('User cancelled photo picker');
      errorMsg = 'User cancelled photo picker';
    } else if (response.error) {
      Logger.log('ImagePicker Error: ', response.error);
      errorMsg = 'ImagePicker Error: ' + response.error;
    } else {
      success = true;
      let iosFileUri = '';
      let androidFileName = '';
      if (Platform.OS === 'ios') {
        iosFileUri = response.uri.substring(5, response.uri.length);
      } else {
        androidFileName = response.path;
      }
      result['upload_uri'] = Platform.OS === 'ios' ? iosFileUri : androidFileName;
    }
    callback({ result, success, errorMsg });
  });
}

export function selectVideo(callback = () => {}, options = {}) {
  let defaultOptions = {
    title: 'Video Picker',
    takePhotoButtonTitle: 'Take Video...',
    mediaType: 'video',
    videoQuality: 'medium',
    noData: true
  };

  let _options = Object.assign({}, defaultOptions, options);

  ImagePicker.showImagePicker(_options, response => {
    Logger.log('Response = ', response);
    let success = false;
    let errorMsg = null;
    let result = { ...response };
    if (response.didCancel) {
      Logger.log('User cancelled video picker');
      errorMsg = 'User cancelled video picker';
    } else if (response.error) {
      Logger.log('ImagePicker Error: ', response.error);
      errorMsg = 'ImagePicker Error: ' + response.error;
    } else {
      success = true;
      let iosFileUri = '';
      let androidFileName = '';
      if (Platform.OS === 'ios') {
        iosFileUri = response.uri.substring(5, response.uri.length);
      } else {
        androidFileName = response.path;
      }
      result['upload_uri'] = Platform.OS === 'ios' ? iosFileUri : androidFileName;
    }
    callback({ result, success, errorMsg });
  });
}

export function readFile(path, encode) {
  return RNFS.readFile(path, encode);
}
