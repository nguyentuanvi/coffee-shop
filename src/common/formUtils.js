import React, { Component } from 'react';
import InputValidator from '../components/InputWithVilidator';

export function renderInput(data, placeholder) {
  const {
    input,
    label,
    type,
    meta: { touched, error, warning }
  } = data;
  return (
    <InputValidator
      touched={touched}
      error={error}
      warning={warning}
      input={input}
      label={label}
      type={type}
      placeholder={placeholder}
      secureTextEntry={input.name === 'password' ? true : false}
      placeholderTextColor="#FFF"
    />
  );
}
