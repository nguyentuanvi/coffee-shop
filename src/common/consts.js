import { Platform } from 'react-native';

// language
export const LANGUAGE_ENGLISH = 'en';
export const LANGUAGE_VIETNAM = 'vn';
export const LANGUAGE_DEFAULT = LANGUAGE_ENGLISH;

// date time
export const FORMAT_DATE_TIME = 'YYYY-MM-DDTHH:mm:ssZ';
export const FORMAT_DATE_TIME_TEXT = 'MMM DD, YYYY hh:mm A';
export const FORMAT_DAY_MONTH_DATE = 'ddd, MMM DD';
export const FORMAT_DATE = 'DD/MM/YYYY';
export const FORMAT_YEAR_MONTH_DAY = 'YYYY-MM-DD';
export const FORMAT_TIME_DATE = 'hh:mm A MMM DD, YYYY';
export const FORMAT_TIME_FILE = 'YYYY-MM-DD-hhmmss';
export const FORMAT_DATE_CALENDAR = 'yyyy-MM-dd';
export const FORMAT_APPOINTMENT_DURATION = 'hh:mm A';
export const FORMAT_APPOINTMENT_DATE = 'ddd, MMM DD, YYYY';

// key AsyncStore
export const KEY_USER_NAME = 'key_user_name';
export const KEY_USER_PASSWORD = 'key_password';
export const USER = 'user';
export const TOKEN = 'token';
export const TOKEN_TYPE = 'token_type';
export const DEVICE_TOKEN = 'DEVICE_TOKEN';

// paging
export const PAGE_LENGTH = 20;

// package name
export const PACKAGE_NAME = 'sampleapp';

//delay
export const DELAY_USERACTION = 500;

//python non field error key
export const PYTHON_NON_FIELD_ERROR = 'non_field_errors';
export const PYTHON_COMMON_ERROR = 'error';

export const DEFAULT_BASE_FONT = 'YanoneKaffeesatz';
export const DEFAULT_FONT =
  Platform.OS === 'ios'
    ? DEFAULT_BASE_FONT + '-Regular'
    : DEFAULT_BASE_FONT + '_regular';
export const DEFAULT_FONT_BOLD =
  Platform.OS === 'ios'
    ? DEFAULT_BASE_FONT + '-Bold'
    : DEFAULT_BASE_FONT + '_bold';

//Google vision
export const GOOGLE_VISION_TYPE_UNSPECIFIED = 'TYPE_UNSPECIFIED';
export const GOOGLE_VISION_FACE_DETECTION = 'FACE_DETECTION';
export const GOOGLE_VISION_LANDMARK_DETECTION = 'LANDMARK_DETECTION';
export const GOOGLE_VISION_LOGO_DETECTION = 'LOGO_DETECTION';
export const GOOGLE_VISION_LABEL_DETECTION = 'LABEL_DETECTION';
export const GOOGLE_VISION_TEXT_DETECTION = 'TEXT_DETECTION';
export const GOOGLE_VISION_DOCUMENT_TEXT_DETECTION = 'DOCUMENT_TEXT_DETECTION';
export const GOOGLE_VISION_SAFE_SEARCH_DETECTION = 'SAFE_SEARCH_DETECTION';
export const GOOGLE_VISION_IMAGE_PROPERTIES = 'IMAGE_PROPERTIES';
export const GOOGLE_VISION_CROP_HINTS = 'CROP_HINTS';
export const GOOGLE_VISION_WEB_DETECTION = 'WEB_DETECTION';
