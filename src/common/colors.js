export const COLOR_WHITE = 'white';
export const COLOR_BLACK = 'black';
export const COLOR_GRAY = 'gray';
export const COLOR_RED = 'rgb(189,92,91)';
export const COLOR_LIGHT_GRAY = 'rgb(230,230,230)';
export const COLOR_TINT_TAB_BAR = 'rgb(212,213,215)';
export const COLOR_TAB_BAR = 'rgb(178,178,178)';
export const COLOR_MENU_HEADER = 'rgb(30,37,49)';
export const COLOR_LABEL = 'rgb(142,142,147)';
export const COLOR_GREEN = 'rgb(131,145,61)';
export const COLOR_YELLOW = 'rgb(192,163,40)';
export const COLOR_BLUE = 'rgb(2,172,236)';
export const COLOR_HOT = '#BD5C5B';
export const COLOR_WARM = '#C0A328';
export const COLOR_COLD = '#A1A1A1';
export const COLOR_APP = '#1E2531';
export const COLOR_APP_HEADER = '#1E2531';
export const COLOR_DISABLE = '#e0e0e0';
export const COLOR_PRIMARY = '#0066ff';
export const COLOR_SECONDARY = '#0066ff';
export const COLOR_BACKDROP = '#000000AA';
export const COLOR_TAB_NAVIGATOR_ACTIVE = 'tomato';
export const COLOR_TAB_NAVIGATOR_INACTIVE = 'black';
export const COLOR_BACKGROUND = 'transparent';


