import * as ActionTypes from './actionTypes';

export function googleVisionRequest(payload) {
  return {
    type: ActionTypes.GOOGLE_VISION_REQUEST,
    payload
  };
}
