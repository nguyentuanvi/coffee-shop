import * as ActionTypes from './actionTypes';

/**
 *
 * @param {*} params
 */
export function login(params) {
  return {
    type: ActionTypes.LOGIN_REQUEST,
    params
  };
}

export function loginFacebook(params) {
  return {
    type: ActionTypes.LOGIN_FACEBOOK_REQUEST,
    params
  };
}

export function loginGoogle(params) {
  return {
    type: ActionTypes.LOGIN_GOOGLE_REQUEST,
    params
  };
}

export function logout() {
  return {
    type: ActionTypes.LOGOUT_REQUEST
  };
}
