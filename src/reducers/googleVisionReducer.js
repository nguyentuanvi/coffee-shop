import { Consts, ActionTypes, Storage } from '../common';

const initiateState = {
  actionType: '',
  imageAnalyze: {
    isLoading: false,
    success: false,
    data: null,
    message: ''
  }
};

export default (state = initiateState, action) => {
  let newState = { ...state };
  newState['actionType'] = action.type;
  switch (action.type) {
    case ActionTypes.GOOGLE_VISION_REQUEST:
      newState['imageAnalyze'] = {
        isLoading: true,
        success: false,
        data: state.imageAnalyze.data,
        message: state.imageAnalyze.message
      };
      return newState;

    case ActionTypes.GOOGLE_VISION_SUCCESS:
      newState['imageAnalyze'] = {
        isLoading: false,
        success: true,
        data: action.data,
        message: ''
      };
      return newState;

    case ActionTypes.GOOGLE_VISION_FAILED:
      newState['imageAnalyze'] = {
        isLoading: false,
        success: false,
        data: null,
        message: ''
      };
      return newState;

    default:
      return newState;
  }
};
