import { combineReducers } from 'redux';

import loginReducer from './loginReducer';
import googleVisionReducer from './googleVisionReducer';

export default combineReducers({
  loginReducer,
  googleVisionReducer
});
