import { setHeaderToken } from '../services/apis/commonApi';
import { Consts, ActionTypes, Storage } from '../common';

const initiateState = {
  actionType: '',
  auth: {
    isLoading: false,
    success: false,
    data: null,
    message: ''
  }
};

export default (state = initiateState, action) => {
  let newState = { ...state };
  newState['actionType'] = action.type;
  switch (action.type) {
    // login
    case ActionTypes.LOGIN_FACEBOOK_REQUEST:
    case ActionTypes.LOGIN_GOOGLE_REQUEST:
    case ActionTypes.LOGIN_REQUEST: {
      newState['auth'] = {
        isLoading: true,
        success: false,
        data: null,
        message: ''
      };
      return newState;
    }

    case ActionTypes.LOGIN_FACEBOOK_SUCCESS:
    case ActionTypes.LOGIN_GOOGLE_SUCCESS:
    case ActionTypes.LOGIN_SUCCESS: {
      let user = action.data ? action.data.data : null;

      if (user) {
        // save user info
        Storage.setDataJson(Consts.USER, user);

        // add token to api
        setHeaderToken(user);
      }

      newState['auth'] = {
        isLoading: false,
        // success: action.data.success,
        data: user,
        message: action.data && action.data.message ? action.data.message : ''
      };

      return newState;
    }

    case ActionTypes.LOGIN_FACEBOOK_FAILED:
    case ActionTypes.LOGIN_GOOGLE_FAILED:
    case ActionTypes.LOGIN_FAILED:
      // clear user info
      Storage.removeData(Consts.USER);

      // remove token at api
      setHeaderToken(null);

      newState['auth'] = {
        isLoading: false,
        success: false,
        data: null,
        message: action.message
      };

      return newState;

    case ActionTypes.LOGOUT_SUCCESS:
      // clear user info
      Storage.removeData(Consts.USER);

      // remove token at api
      setHeaderToken(null);

      newState['auth'] = {
        isLoading: false,
        success: true,
        data: null,
        message: null
      };

      return newState;

    case ActionTypes.LOGOUT_FAILED:
      newState['auth'] = {
        isLoading: false,
        success: false,
        data: state.auth.data,
        message: action.message
      };
      return newState;

    default:
      return newState;
  }
};
