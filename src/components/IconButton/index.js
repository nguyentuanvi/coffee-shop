import React, { PureComponent } from 'react';
import { Button } from 'react-native-elements';
import { Consts, Utils, Colors } from '../../common';
import styles from './styles';
import AbstractButton from '../AbstractButton';

let timeoutId = -1;

class IconButton extends AbstractButton {
  constructor(props) {
    super(props);
    this.state = {
      useraction: false
    };
  }

  render() {
    let touchable;
    touchable = (
      <Button
        buttonStyle={styles.containerStyle}
        backgroundColor={Colors.COLOR_SECONDARY}
        {...this.props}
        onPress={() => {
          this.onPress();
        }}
      />
    );

    return touchable;
  }
}

export default IconButton;
