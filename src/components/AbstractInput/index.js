import React, { PureComponent } from 'react';

import { PureAbstractView } from '../AbstractView';
import { Logger, Utils } from '../../common';

export default class AbstractInput extends PureAbstractView {
  constructor(props) {
    super(props);
    const { validators } = props;
    this.state = {
      error: undefined,
      valid: !(validators && validators.length > 0)
    };
  }

  isValid() {
    return this.state.valid;
  }

  validate(text) {
    const { validators, onValidatationChanged } = this.props;
    let error;
    for (let validator of validators) {
      error = validator(text);
      Logger.log(error);
      if (error) {
        this.setState({
          error: error,
          valid: false
        });
        if (Utils.isFunction(onValidatationChanged)) {
          onValidatationChanged(false);
        }
        return false;
      }
    }

    this.setState({
      valid: true,
      error: undefined
    });
    if (Utils.isFunction(onValidatationChanged)) {
      onValidatationChanged(true);      
    }
    return true;
  }

  render() {
    return null;
  }
}
