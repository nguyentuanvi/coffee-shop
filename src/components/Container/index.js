import React from 'react';
import { View, Dimensions, StyleSheet } from 'react-native';
import { Colors } from '../../common';
import { PureAbstractView } from '../AbstractView';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default class Container extends PureAbstractView {
  render() {
    const _style = this.computeStyle(styles.container, this.props.style);
    return (
      <View {...this.props} style={_style}>
        {this.props.children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    height: height,
    backgroundColor: Colors.COLOR_BACKGROUND
  }
});

Container.defaultProps = {
  style: {}
};
