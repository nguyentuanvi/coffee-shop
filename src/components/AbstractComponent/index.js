import React, { Component } from 'react';

export default class AbstractComponent extends Component {
  constructor(props) {
    super(props);

    this.props.navigation.addListener('didFocus', payload => {});

    this.props.navigation.addListener('didBlur', payload => {});
  }

  shouldComponentUpdate(nextProps, nextStates) {
    if (this.props.navigation.isFocused()) {
      return true;
    }
    return false;
  }

  render() {
    return null;
  }
}
