// @flow
import React, { Component } from 'react';
import {
  View,
  ActivityIndicator,
  Modal,
  TouchableWithoutFeedback,
  Text
} from 'react-native';
import {} from 'native-base';
import { Colors, Utils, I18n } from '../../common';

class Loading extends Component {
  render() {
    const {
      onRequestClose,
      onOutsidePress,
      transparent,
      animationType,
      message
    } = this.props;
    return (
      <Modal
        visible={this.props.visible}
        animationType={animationType ? animationType : 'none'}
        onRequestClose={() => {
          if (Utils.isFunction(onRequestClose)) {
            onRequestClose();
          }
        }}
        transparent={transparent ? transparent : true}
      >
        <TouchableWithoutFeedback
          onPress={() => {
            if (Utils.isFunction(onOutsidePress)) {
              onOutsidePress();
            }
          }}
        >
          <View style={styles.container}>
            <ActivityIndicator
              animating={this.props.animating}
              color={Colors.COLOR_YELLOW}
              size="large"
            />
            <Text style={[styles.text]}>
              {message ? message : I18n.t('common.loading')}
            </Text>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.COLOR_BACKDROP
  },

  text: {
    marginTop: 20,
    fontSize: 18,
    color: Colors.COLOR_YELLOW
  }
};

export default Loading;
