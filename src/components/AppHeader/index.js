// @flow
import React, { Component, PureComponent } from 'react';
import { View, Platform } from 'react-native';

import {
  Header,
  Left,
  Right,
  Body,
  Button,
  Title,
  Subtitle
} from 'native-base';

import { Icon } from 'react-native-elements';

import TouchableEffect from '../Touchable';
import AppText from '../AppText';

import styles from './styles';
import { Dimens } from '../../common';

export default class AppHeader extends PureComponent {
  render() {
    const {
      showLeftButton,
      showRightButton,
      leftIcon,
      rightIcon,
      title,
      subtitle,
      renderLeftChild,
      renderRightChild,
      onLeftPressed,
      onRightPressed,
      drawer,
      headerStyle,
      iconLeftStyle,
      iconRightStyle,
      titleStyle,
      subtitleStyle,
      leftText,
      iconType,
      iconLeftColor,
      iconRightColor,
      showBackTextOnAndroid
    } = this.props;

    let leftTextChild = (
      <AppText
        text={leftText}
        style={[styles.text, { marginLeft: Dimens.PADDING_SMALL }]}
      />
    );

    let leftChild = renderLeftChild
      ? renderLeftChild()
      : showLeftButton && (
          <TouchableEffect
            transparent
            onPress={() =>
              onLeftPressed
                ? onLeftPressed()
                : drawer
                  ? this.props.navigation.toggleDrawer()
                  : this.props.navigation.goBack()
            }
          >
            <View style={styles.buttonContentLeft}>
              <Icon
                name={drawer ? 'ios-menu' : leftIcon}
                style={[styles.iconLeft, iconLeftStyle]}
                type={iconType}
                color={iconLeftColor}
              />
              {leftText &&
                (Platform.OS === 'android'
                  ? showBackTextOnAndroid
                    ? leftTextChild
                    : null
                  : leftTextChild)}
            </View>
          </TouchableEffect>
        );

    let rightChild = renderRightChild
      ? renderLeftChild()
      : showRightButton && (
          <TouchableEffect transparent onPress={() => onRightPressed()}>
            <Icon
              name={rightIcon}
              style={[styles.iconRight, iconRightStyle]}
              type={iconType}
              color={iconRightColor}
            />
          </TouchableEffect>
        );

    return (
      <Header style={[styles.header, headerStyle]}>
        <Left style={{ flex: 0.5 }}>{leftChild}</Left>
        <Body style={{ flex: 1 }}>
          <Title style={[styles.text, styles.textTitle, titleStyle]}>
            {title}
          </Title>
          {subtitle && (
            <Subtitle style={[styles.text, subtitleStyle]}>{subtitle}</Subtitle>
          )}
        </Body>
        <Right style={{ flex: 0.5 }}>{rightChild}</Right>
      </Header>
    );
  }
}

AppHeader.defaultProps = {
  showLeftButton: true,
  showRightButton: false,
  leftIcon: 'ios-arrow-back',
  rightIcon: 'notifications',
  title: 'Nav title',
  drawer: false,
  subtitle: null,
  renderLeftChild: null,
  renderRightChild: null,
  onLeftPressed: null,
  onRightPressed: () => {},
  headerStyle: {},
  iconLeftStyle: {},
  iconRightStyle: {},
  titleStyle: {},
  subtitleStyle: {},
  iconType: 'ionicon',
  iconRightColor: 'white',
  iconLeftColor: 'white',
  showBackTextOnAndroid: false
};
