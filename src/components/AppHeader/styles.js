import { StyleSheet } from 'react-native';
import { Colors, Consts } from '../../common';

const styles = StyleSheet.create({
  header: {
    backgroundColor: Colors.COLOR_PRIMARY
  },
  iconLeft: {
    color: 'white'
  },
  iconRight: {
    color: 'white'
  },
  text: {
    color: 'white',
    fontFamily: Consts.DEFAULT_FONT
  },
  textTitle: {
    fontFamily: Consts.DEFAULT_FONT_BOLD
  },
  button: {
    flex: 1
  },
  buttonRight: {
    alignSelf: 'flex-end'
  },
  buttonContentLeft: {
    flexDirection: 'row',
    alignItems: 'center'
  }
});

export default styles;
