import React, { PureComponent } from 'react';

import { PureAbstractView } from '../AbstractView';
import { Consts, Utils } from '../../common';

class AbstractButton extends PureAbstractView {
  constructor(props) {
    super(props);
    this.state = {
      useraction: false,
      timeoutId: -1
    };
  }

  componentWillUnmount() {
    if (this.state.timeoutId !== -1) {
      clearTimeout(this.state.timeoutId);
    }
  }

  onPress() {
    const { delay } = this.props;

    if (!this.state.useraction) {
      this.setState({
        useraction: true
      });
      if (this.state.timeoutId !== -1) {
        clearTimeout(this.state.timeoutId);
      }
      let timeoutId = setTimeout(() => {
        this.setState({
          useraction: false
        });
      }, delay ? delay : Consts.DELAY_USERACTION);      

      const { onPress } = this.props;
      if (Utils.isFunction(onPress)) {
        onPress();
      }

      this.setState({ timeoutId: timeoutId });
    }
  }

  render() {
    null;
  }
}

AbstractButton.defaultProps = {
  style: {}
};

export default AbstractButton;
