import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';

export default class DrawerButton extends Component {
  render() {
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('DrawerToggle');
          }}
        >
          <Icon name="menu" />
        </TouchableOpacity>
      </View>
    );
  }
}
