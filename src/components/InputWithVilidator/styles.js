import EStyleSheet from 'react-native-extended-stylesheet';
import { Platform } from 'react-native';

import * as Colors from '../../common/colors';
import * as Consts from '../../common/consts';

export default EStyleSheet.create({
  inputGrp: {
    flexDirection: 'row',
    backgroundColor: 'lightgray',
    marginBottom: 5,
    borderWidth: 0,
    borderColor: 'transparent'
  },
  input: {
    paddingLeft: 10,
    color: '#000',
    fontFamily: Consts.DEFAULT_FONT
  },
  form: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20
  },
  formErrorIcon: {
    color: '#000',
    marginTop: 5,
    right: 10
  },
  formErrorText1: {
    fontSize: Platform.OS === 'android' ? 12 : 15,
    color: Colors.COLOR_RED,
    textAlign: 'right',
    top: -10
  },
  formErrorText2: {
    fontSize: Platform.OS === 'android' ? 12 : 15,
    color: 'transparent',
    textAlign: 'right',
    top: -10
  }
});
