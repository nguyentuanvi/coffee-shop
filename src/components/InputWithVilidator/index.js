import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';
import { Icon, Input, Item } from 'native-base';

import { Colors, Utils, Logger } from '../../common';

import AbstractInput from '../AbstractInput';

import AppText from '../AppText';

import styles from './styles';

export default class InputValidator extends AbstractInput {
  render() {
    const {
      icon,
      input,
      containerStyle,
      inputStyle,
      iconStyle,
      textErrorStyle,
      placeholder,
      secureTextEntry,
      onSubmitEditing,
      returnKeyType,
      returnKeyLabel,
      onChangeText,
      value
    } = this.props;

    const { error, blur } = this.state;

    const _containerStyle = this.computeStyle(styles.inputGrp, containerStyle);
    const _iconStyle = this.computeStyle({ color: Colors.COLOR_BLACK, marginLeft: 10 }, iconStyle);
    const _inputStyle = this.computeStyle(styles.input, inputStyle);
    const _textErrorStyle = this.computeStyle(styles.formErrorText1, textErrorStyle);

    return (
      <View>
        <Item error={error && blur} style={_containerStyle}>
          <Icon active name={icon} style={_iconStyle} />
          <Input
            ref={c => {
              this.textInput = c;
            }}
            placeholderTextColor="gray"
            style={_inputStyle}
            placeholder={placeholder}
            secureTextEntry={secureTextEntry}
            onSubmitEditing={onSubmitEditing}
            returnKeyType={returnKeyType}
            returnKeyLabel={returnKeyLabel}
            onChangeText={onChangeText}
            onBlur={() => {
              this.validate(value);
              this.setState({ blur: false });
            }}
            onFocus={() => {
              this.setState({ blur: true });
            }}
            value={value}
            {...input}
          />
          {error && !blur ? (
            <Icon
              active
              style={styles.formErrorIcon}
              onPress={() => {
                this.textInput._root.clear();
                this.setState({
                  blur: false
                });
              }}
              name="close"
            />
          ) : (
            <Text />
          )}
        </Item>
        {error && !blur ? <AppText style={_textErrorStyle} text={error} /> : null}
      </View>
    );
  }
}

InputValidator.defaultProps = {
  inputStyle: {},
  iconStyle: {},
  containerStyle: {},
  textErrorStyle: {},
  onSubmitEditing: () => {},
  returnKeyType: 'default',
  returnKeyLabel: '',
  onChangeText: () => {},
  validators: [],
  onValidatationChanged: valid => {}
};
