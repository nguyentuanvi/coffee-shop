import React from 'react';
import { Dimensions, StyleSheet, ScrollView, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { Dimens } from '../../common';
import { PureAbstractView } from '../AbstractView';

// const width = Dimensions.get('window').width;
// const height = Dimensions.get('window').height;

export default class Content extends PureAbstractView {
  render() {
    const _style = this.computeStyle(styles.container, this.props.style);
    const _contentContainerStyle = this.computeStyle(
      { padding: this.props.padder ? Dimens.contentPadding : 0 },
      this.props.contentContainerStyle
    );
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
        }}
      >
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          {...this.props}
          style={_style}
          contentContainerStyle={_contentContainerStyle}
        >
          {this.props.children}
        </ScrollView>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent'
  }
});

Content.defaultProps = {
  style: {},
  contentContainerStyle: {}
};
