import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, StatusBar, View, Text, Image, Vibration ,Dimensions} from 'react-native';
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures';
import AppText from '../AppText'

const deviceWidth = Dimensions.get("window").width

const styles = {
  root: {
    flex: 1,
    backgroundColor: '#f2f4f7',
  },
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  content: {
    flex: 1,
    flexDirection: 'row',
  },
  iconApp: {
    marginTop: 10,
    marginLeft: 20,
    resizeMode: 'contain',
    width: 24,
    height: 24,
    borderRadius: 5,
  },
  icon: {
    marginTop: 10,
    marginLeft: 10,
    resizeMode: 'contain',
    width: 48,
    height: 48,
  },
  textContainer: {
    alignSelf: 'center',
    marginLeft: 20,
    paddingTop: 5,
    paddingBottom: 5,
  },
  title: {
    marginTop: 5,
    color: 'black',
    fontWeight: 'bold',
  },
  message: {
    color: 'black',
    marginTop: 5,
    marginRight: 5,
  },
  footer: {
    backgroundColor: '#696969',
    borderRadius: 5,
    alignSelf: 'center',
    height: 5,
    width: 35,
    margin: 5,
  },
};

class DefaultInAppNotificationBody extends React.Component {
  constructor() {
    super();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isOpen !== this.props.isOpen) {
      StatusBar.setHidden(nextProps.isOpen);
    }

    // if ((this.props.vibrate || nextProps.vibrate) && nextProps.isOpen && !this.props.isOpen) {
    //   Vibration.vibrate();
    // }
  }

  onSwipe(direction) {
    console.log("=====> SWIPE,",direction)
    const { SWIPE_UP } = swipeDirections;
      this.props.onClose();
  }

  renderIcon() {
    const {
      iconApp,
      icon,
    } = this.props;

    if (icon) {
      return <Image source={icon} style={styles.icon} />;
    } else if (iconApp) {
      return <Image source={iconApp} style={styles.iconApp} />;
    }

    return null;
  }

  render() {
    const {
      title,
      message,
      onPress,
      onClose,
    } = this.props;

    return (
      <GestureRecognizer  onSwipe={(state) => onClose()} style={styles.container}>
      <View style={styles.root}>
          <TouchableOpacity
            style={styles.content}
            activeOpacity={1}
            underlayColor="transparent"
            onPress={() => {
              onClose();
              onPress();
            }}
          >
            {this.renderIcon()}
            <View style={styles.textContainer}>
              <AppText numberOfLines={1} style={styles.title}>{title}</AppText>
              <AppText numberOfLines={2} style={styles.message}>{message}</AppText>
            </View>
          </TouchableOpacity>
          <View style={styles.footer} />
      </View>
      </GestureRecognizer>
    );
  }
}

DefaultInAppNotificationBody.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  vibrate: PropTypes.bool,
  isOpen: PropTypes.bool,
  onPress: PropTypes.func,
  onClose: PropTypes.func,
  iconApp: Image.propTypes.source,
  icon: Image.propTypes.source,
};

DefaultInAppNotificationBody.defaultProps = {
  title: 'Notification',
  message: 'This is a test notification',
  vibrate: true,
  isOpen: false,
  iconApp: null,
  icon: null,
  onPress: () => null,
  onClose: () => null,
};

export default DefaultInAppNotificationBody;
