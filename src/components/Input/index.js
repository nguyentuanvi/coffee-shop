import React from 'react';
import { StyleSheet, TextInput } from 'react-native';

import { Consts, Dimens } from '../../common';

import AbstractInput from '../AbstractInput';

export default class Input extends AbstractInput {
  render() {
    const _style = this.computeStyle(styles.input, this.props.style);
    return (
      <TextInput
        ref={ref => {
          this._root = ref;
        }}
        placeholderTextColor={'white'}
        underlineColorAndroid={'transparent'}
        allowFontScaling={false}
        {...this.props}
        style={_style}
      />
    );
  }
}

const styles = StyleSheet.create({
  input: {
    color: 'white',
    fontFamily: Consts.DEFAULT_FONT,
    backgroundColor: 'transparent',
    paddingVertical: 5,
    fontSize: Dimens.fontSizeBase
  }
});

Input.defaultProps = {
  style: {}
};
