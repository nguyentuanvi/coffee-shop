// @flow
import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import { PureAbstractView } from '../AbstractView';
import { AppStyles } from '../../common/';
import * as Consts from '../../common/consts';

export default class AppText extends PureAbstractView {
  render() {
    const _style = this.computeStyle(
      AppStyles.fontContent,
      styles.font,
      this.props.style
    );
    return (
      <Text {...this.props} allowFontScaling={false} style={_style}>
        {this.props.text}
      </Text>
    );
  }
}

const styles = StyleSheet.create({
  font: {
    fontFamily: Consts.DEFAULT_FONT
  }
});

AppText.defaultProps = {
  text: '',
  style: {}
};
