import React, { PureComponent } from 'react';
import { StyleSheet } from 'react-native';
import { Button } from 'native-base';
import { Consts, Dimens, Utils, Colors, Logger } from '../../common';
import AbstractButton from '../AbstractButton';

let timeoutId = -1;

class TouchableEffect extends AbstractButton {
  constructor(props) {
    super(props);
    this.state = {
      useraction: false
    };
  }

  render() {
    let touchable;
    const { center } = this.props;
    const _style = this.computeStyle({ alignSelf: center ? 'center' : null }, styles.button, this.props.style);    
    touchable = (
      <Button
        {...this.props}
        style={_style}
        activeOpacity={Dimens.OPACITY}
        onPress={() => {
          this.onPress();
        }}
      />
    );

    return touchable;
  }
}

const styles = StyleSheet.create({
  button: {
    padding: Dimens.PADDING_CONTENT,
    backgroundColor: Colors.COLOR_PRIMARY
  }
});

export default TouchableEffect;
