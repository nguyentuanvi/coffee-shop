import AbstractView from './AbstractView';
import PureAbstractView from './PureAbstractView';

export { AbstractView, PureAbstractView };
