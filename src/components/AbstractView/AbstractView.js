import React from 'react';
import { _computeStyle } from './AbstractViewUtils';

export default class AbstractView extends React.Component {
  computeStyle(...args) {
    return _computeStyle(args);
  }

  render() {
    return null;
  }
}
