// @flow
import React, { Component } from 'react';
import {  } from 'react-native';
import { createImageProgress } from 'react-native-image-progress';
import FastImage from 'react-native-fast-image';
import * as Progress from 'react-native-progress';
import { Logger } from '../../common';

const ImageProgress = createImageProgress(FastImage);

export default class AppImage extends Component {
  render() {
    return (
      <ImageProgress
        indicator={Progress.Pie}
        indicatorProps={{
          size: 80,
          borderWidth: 0,
          color: 'rgba(150, 150, 150, 1)',
          unfilledColor: 'rgba(200, 200, 200, 0.2)'
        }}
        resizeMode={FastImage.resizeMode.contain}
        {...this.props}
      />
    );
  }
}
