import React from 'react';
import { View, StyleSheet } from 'react-native';

import { PureAbstractView } from '../AbstractView';

export default class InputContainer extends PureAbstractView {
  render() {
    const _style = this.computeStyle(styles.inputContainer, this.props.style);
    return (
      <View {...this.props} style={_style}>
        {this.props.children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    borderRadius: 8,
    borderColor: 'white',
    borderWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 10
  }
});

InputContainer.defaultProps = {
  style: {}
};
