export default {
  login: {
    title: 'Login',
    emailLabel: 'Email',
    passLabel: 'Password',
    getStarted: 'Get Started',
    loginFb: 'Login Facebook',
    loginGg: 'Login Google',
    loginPhone: 'Login by phone number',
    createAccount: 'Create Account',
    forgotPass: 'Forgot password',
    phoneLabel: 'Phone number (+84..)',
    codeLabel: 'Verify code',
    sendCode: 'Send code',
    sendingCode: 'Sending code',
    verifyCode: 'Verify',
    verifyingCode: 'Verifying',
    phoneLoginNavTitle: 'Phone Login',
    zaloLogin: 'Login Zalo'
  },
  home: {
    title: 'Home'
  },
  common: {
    loading: 'Loading'
  },
  apptitle: {
    login: 'Login',
    home: 'Home',
    calendars: 'Calendars',
    calendarList: 'Calendar List',
    agenda: 'Agenda',
    share: 'Share',
    tform: 'T Form',
    chat: 'Chat',
    collapsible: 'Collapsible',
    image: 'Fast&Progress Image',
    maps: 'Maps',
    credit: 'Credit Card'
  },
  sidebar: {
    calendars: 'Calendars',
    calendarList: 'Calendar List',
    agenda: 'Agenda',
    home: 'Home',
    share: 'Share',
    tform: 'T Form',
    chat: 'Chat',
    collapsible: 'Collapsible',
    image: 'Fast&Progress Image',
    maps: 'Maps',
    credit: 'Credit Card'
  },
  tab2: {
    showPicker: 'Show datepicker'
  }
};
