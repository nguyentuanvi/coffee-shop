import I18n from 'react-native-i18n';
import en from './en';
import vn from './vn';
import * as Consts from '../../common/consts';

I18n.fallbacks = true;
I18n.translations = { en, vn };
I18n.locale = Consts.LANGUAGE_DEFAULT;

export default I18n;
