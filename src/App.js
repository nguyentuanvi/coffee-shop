import React from 'react';
import { Platform } from 'react-native';
import {
  createBottomTabNavigator,
  createDrawerNavigator,
  createStackNavigator,
  createSwitchNavigator
} from 'react-navigation';
import { Root } from 'native-base';
import { Icon } from 'react-native-elements';
import t from 'tcomb-form-native';

import * as Colors from './common/colors';
import * as Consts from './common/consts';
import { I18n, Dimens } from './common';
import DrawerButton from './components/DrawerButton';

import Login from './screens/Login/';
import PhoneLogin from './screens/PhoneLogin';
import Sidebar from './screens/Sidebar';
import TabA from './screens/TabA';
import TabB from './screens/TabB';
import TabC from './screens/TabC';
import Camera from './screens/Camera';
import Calendars from './screens/Calendars';
import MyCalendarList from './screens/CalendarList';
import Agenda from './screens/Agenda';
import Share from './screens/Share';
import TForm from './screens/TCombForm';
import Chat from './screens/Chat';
import Collapsible from './screens/Collapsible';
import ImageScreen from './screens/ImageScreen';
import AuthLoading from './screens/AuthLoading';
import Maps from './screens/Maps';
import CreditCard from './screens/CreditCard';

/**
 * Config TComb Form style
 */
// Label
t.form.Form.stylesheet.controlLabel.normal.fontFamily =
  Consts.DEFAULT_FONT_BOLD; // This does NOT apply!
t.form.Form.stylesheet.controlLabel.error.fontFamily = Consts.DEFAULT_FONT_BOLD; // This does NOT apply!
t.form.Form.stylesheet.controlLabel.normal.fontWeight = null; // This does NOT apply!
t.form.Form.stylesheet.controlLabel.error.fontWeight = null; // This does NOT apply!

// Textbox
t.form.Form.stylesheet.textbox.normal.fontFamily = Consts.DEFAULT_FONT; // This does apply!
t.form.Form.stylesheet.textbox.error.fontFamily = Consts.DEFAULT_FONT; // This does apply!
t.form.Form.stylesheet.textbox.notEditable.fontFamily = Consts.DEFAULT_FONT; // This does apply!

//date picker
t.form.Form.stylesheet.dateValue.normal.fontFamily = Consts.DEFAULT_FONT; // This does apply!
t.form.Form.stylesheet.dateValue.normal.fontFamily = Consts.DEFAULT_FONT; // This does apply!

//select
t.form.Form.stylesheet.pickerValue.normal.fontFamily = Consts.DEFAULT_FONT; // This does apply!
t.form.Form.stylesheet.pickerValue.error.fontFamily = Consts.DEFAULT_FONT; // This does apply!

//button
t.form.Form.stylesheet.buttonText.fontFamily = Consts.DEFAULT_FONT; // This does apply!

//error block
t.form.Form.stylesheet.errorBlock.fontFamily = Consts.DEFAULT_FONT; // This does apply!

//help block
t.form.Form.stylesheet.helpBlock.normal.fontFamily = Consts.DEFAULT_FONT; // This does apply!
t.form.Form.stylesheet.helpBlock.error.fontFamily = Consts.DEFAULT_FONT; // This does apply!

/**
 * App Navigator
 */

const Tabs = createBottomTabNavigator(
  {
    TabA: {
      screen: TabA,
      navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused }) => (
          <Icon
            name="camera"
            size={Dimens.SIZE_ICON_TAB_NAVIGATOR}
            color={
              focused
                ? Colors.COLOR_TAB_NAVIGATOR_ACTIVE
                : Colors.COLOR_TAB_NAVIGATOR_INACTIVE
            }
          />
        )
      })
    },
    TabB: {
      screen: TabB,
      navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused }) => (
          <Icon
            name="camera"
            size={Dimens.SIZE_ICON_TAB_NAVIGATOR}
            color={
              focused
                ? Colors.COLOR_TAB_NAVIGATOR_ACTIVE
                : Colors.COLOR_TAB_NAVIGATOR_INACTIVE
            }
          />
        )
      })
    },
    TabC: {
      screen: TabC,
      navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused }) => (
          <Icon
            name="camera"
            size={Dimens.SIZE_ICON_TAB_NAVIGATOR}
            color={
              focused
                ? Colors.COLOR_TAB_NAVIGATOR_ACTIVE
                : Colors.COLOR_TAB_NAVIGATOR_INACTIVE
            }
          />
        )
      })
    }
  },
  {
    order: ['TabA', 'TabB', 'TabC'],
    initialRouteName: 'TabA',
    tabBarPosition: 'bottom',
    swipeEnabled: false,
    animationEnabled: false,
    lazy: true,
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
      showIcon: true,
      showLabel: true,
      allowFontScaling: false,
      labelStyle: {
        fontSize: Dimens.SIZE_TEXT_TAB_NAVIGATOR,
        fontFamily: Consts.DEFAULT_FONT
      },
      style: {
        height: Platform.OS === 'ios' ? 48 : 65 // I didn't use this in my app, so the numbers may be off.
      }
    }
  }
);

const Drawer = createDrawerNavigator(
  {
    Home: {
      screen: Tabs
    },
    Calendars: {
      screen: Calendars
    },
    CalendarList: {
      screen: MyCalendarList
    },
    Agenda: {
      screen: Agenda
    },
    Share: {
      screen: Share
    },
    TForm: {
      screen: TForm
    },
    Chat: {
      screen: Chat
    },
    Collapsible: {
      screen: Collapsible
    },
    Image: {
      screen: ImageScreen
    },
    Maps: {
      screen: Maps
    },
    CreditCard: {
      screen: CreditCard
    }
  },
  {
    initialRouteName: 'Home',
    contentComponent: props => <Sidebar {...props} />,
    drawerPosition: 'left'
  }
);

const AuthStack = createStackNavigator(
  {
    Login: {
      screen: Login
    },
    PhoneLogin: {
      screen: PhoneLogin
    },
    Drawer: {
      screen: Drawer
    }
  },
  {
    index: 0,
    mode: 'modal',
    initialRouteName: 'Login',
    headerMode: 'none',
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);

const App = createStackNavigator(
  {
    Drawer: {
      screen: Drawer
    },
    Camera: {
      screen: Camera
    },
    AuthStack: {
      screen: AuthStack
    }
  },
  {
    index: 0,
    mode: 'modal',
    initialRouteName: 'Drawer',
    headerMode: 'none',
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);

const Switch = createSwitchNavigator(
  {
    AuthLoading: AuthLoading,
    App: App,
    Auth: AuthStack
  },
  {
    initialRouteName: 'AuthLoading'
  }
);

export default () => (
  <Root>
    <Switch />
  </Root>
);
