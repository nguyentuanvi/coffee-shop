import I18n from 'react-native-i18n';

/**
 * response {
 ** data - the object originally from the server that you might wanna mess with duration - the number of milliseconds
 ** problem - the problem code (see the bottom for the list)
 ** ok - true or false
 ** status - the HTTP status code
 ** headers - the HTTP response headers
 ** config - the underlying axios config for the request
 ** }
 * @param {*} response
 */
export const processResponseService = response => {  
  let message = response.status && response.status >= 300 ? response.data : null;
  let errorMsg = '';
  if (message) {
    for (let key in message) {
      if (key === 'non_field_errors') {
        let errors = message[key];
        for (let error of errors) {
          errorMsg += error + '\n';
        }
      } else {
        let errors = message[key];
        for (let error of errors) {
          errorMsg += key + ':' + error + '\n';
        }
      }
    }

    if (errorMsg.length > 0) {
      errorMsg = errorMsg.substr(0, errorMsg.length - 1);
    }
  } else {
    errorMsg = response.problem;
  }
  return {
    status: response.ok,
    data: response.data,
    message: errorMsg
  };
};

/**
 * get form data from params
 * @param {*} params
 */
export const getFormData = params => {
  var formDatas = new FormData();
  Object.keys(params).map(key =>
    formDatas.append(key, encodeURIComponent(params[key]))
  );
  return formDatas;
};

/**
 * get header for url encoded
 */
export const getHeaderFormUrlEncoded = () => ({
  'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
});


export const getMultiPartHeader = () => ({
  'Content-Type': 'multipart/form-data'
});

/**
 * get header for application json
 */
export const getHeaderApplicationJson = () => ({
  'Content-Type': 'application/json',
});
