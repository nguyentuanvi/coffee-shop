import * as Consts from '../common/consts';

// timeout
export const API_TIMEOUT = 30 * 1000; // 30 seconds
export const FILE_TIMEOUT = 2 * 60 * 1000; // 2 minutes

export const API_KEY = 'AIzaSyCdKU4SRJv08y3IHLzoxP82fXuFKkotDwc';
export const GOOGLE_VISION_API = `https://vision.googleapis.com/v1/images:annotate?key=${API_KEY}`;

// url
export const BASE_URL = 'http://127.0.0.1:8000';
// export const BASE_URL = 'http://112.213.94.138/ibc';

export const API_URL = BASE_URL + '/api/';

export const TOKEN_TYPE = 'Token';

// auth
export const LOGIN_URL = API_URL + 'auth/login';
export const LOGIN_FACEBOOK_URL = API_URL + 'auth/login_facebook';
export const LOGIN_GOOGLE_URL = API_URL + 'auth/login_google';

export const UPDATE_PROFILE_URL = userId => API_URL + `users/${userId}`;
