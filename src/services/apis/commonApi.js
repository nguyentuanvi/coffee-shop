import { create } from 'apisauce';
import * as Configs from '../configs';
import * as ServiceUtility from '../serviceUtility';
import RNFetchBlob from 'react-native-fetch-blob';
import { Storage, Consts } from '../../common';

const API = create({
  baseURL: Configs.BASE_URL,
  timeout: Configs.API_TIMEOUT,
  headers: ServiceUtility.getHeaderApplicationJson()
});

API.addRequestTransform(transform => {
  console.log('==========================start-transform=============================');
  console.log(transform);
  console.log('==========================end-transform=============================');
});

// Monitor API
API.addMonitor(response => {
  console.log('============================START==========================');
  console.log(response);
  console.log('============================END============================');
});

/**
 * update header to api
 * @param {*} user
 */
export const setHeaderToken = user => {
  if (user && user.auth_token) {
    API.setHeader('Authorization', `${Configs.TOKEN_TYPE} ${user.auth_token.key}`);    
  } else {
    API.deleteHeader('Authorization');
  }
};

/**
 * api form data
 * @param {*} url
 * @param {*} params
 * @param {*} onProgress: (progress) => { }
 * @param {*} onStart: () => { }
 * @param {*} onSuccess: (data) => { }
 * @param {*} onFail: (message, statusCode) => { }
 */
export const apiUploadFile = async (method = 'POST', url, params, onProgress, onStart, onSuccess, onFail) => {
  try {
    console.log('url:', url);
    console.log('data: ', params);

    // onstart listener
    if (onStart) onStart();

    let headers = ServiceUtility.getMultiPartHeader();
    let userInfo = await Storage.getDataJson(Consts.USER);
    headers['Authorization'] = Configs.TOKEN_TYPE + ' ' + userInfo.auth_token.key;

    // api
    RNFetchBlob.config({ timeout: Configs.FILE_TIMEOUT })
      .fetch(method, url, headers, params)
      .uploadProgress({ interval: 250 }, (written, total) => {
        console.log('apiUploadFile ==> progress::', written / total);
        if (onProgress) onProgress(written / total);
      })
      .then(resp => {
        // the following conversions are done in js, it's SYNC
        console.log('apiUploadFile ==> resp::', resp);
        // on success listener
        if (onSuccess) onSuccess(resp.json());
      })
      .catch((errorMessage, statusCode) => {
        // error handling
        console.log('apiUploadFile ==> errorMessage::' + errorMessage + ', statusCode:: ' + statusCode);
        // on fail listener
        if (onFail) onFail(errorMessage, statusCode);
      });
  } catch (error) {
    console.log('apiUploadFile ==> error::', error);
  }
};

export const wrapFileData = path => {
  let data = '';
  if (path.startsWith('file://')) {
    data = 'RNFetchBlob-' + path;
  } else {
    data = RNFetchBlob.wrap(path);
  }

  return data;
};

export default API;
