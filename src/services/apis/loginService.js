import API from '../apis/commonApi';
import * as Configs from '../configs';

export const loginFunction = params => API.post(Configs.LOGIN_URL, params);

export const loginFacebookFunction = params => API.post(Configs.LOGIN_FACEBOOK_URL, params);

export const loginGoogleFunction = params => API.post(Configs.LOGIN_GOOGLE_URL, params);
