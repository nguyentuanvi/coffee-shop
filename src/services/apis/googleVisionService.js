import API from '../apis/commonApi';
import * as Configs from '../configs';

export const analyzeImage = payload =>
  API.post(Configs.GOOGLE_VISION_API, payload);
