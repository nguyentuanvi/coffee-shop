import { call, put, takeLatest } from 'redux-saga/effects';
import * as GoogleVision from '../apis/googleVisionService';
import * as ActionTypes from '../../actions/actionTypes';
import * as Consts from '../../common/consts';
import { Logger } from '../../common';

function* googleVision(action) {
  try {
    const data = {
      requests: [
        {
          image: {
            content: action.payload
          },
          features: [
            {
              type: Consts.GOOGLE_VISION_LABEL_DETECTION
            }
          ]
        }
      ]
    };

    const response = yield call(
      GoogleVision.analyzeImage,
      JSON.stringify(data)
    );

    Logger.log(response);

    // if (status) {
    //   yield put({ type: ActionTypes.LOGIN_SUCCESS, data: data });
    // } else {
    //   yield put({ type: ActionTypes.LOGIN_FAILED, message: message });
    // }
  } catch (error) {
    yield put({ type: ActionTypes.LOGIN_FAILED, message: error });
  }
}

function* googleVisionSagas() {
  yield takeLatest(ActionTypes.GOOGLE_VISION_REQUEST, googleVision);
}

export default googleVisionSagas;
