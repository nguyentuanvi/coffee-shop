import { /*put, takeEvery,*/ all } from 'redux-saga/effects';
import loginSagas from '../sagas/loginSagas';
import googleVisionSagas from '../sagas/googleVisionSagas';

export default function* rootSagas() {
  yield all([loginSagas(), googleVisionSagas()]);
}
