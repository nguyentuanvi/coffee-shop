import { call, put, takeLatest } from 'redux-saga/effects';
import * as LoginService from '../apis/loginService';
import * as ActionTypes from '../../actions/actionTypes';
import * as ServiceUtility from '../serviceUtility';
import { setHeaderToken } from '../apis/commonApi';
import QS from 'qs';

function* login(payload) {
  try {
    setHeaderToken(null);

    const { status, data, message } = ServiceUtility.processResponseService(
      yield call(LoginService.loginFunction, JSON.stringify(payload.params))
    );
    if (status) {
      yield put({ type: ActionTypes.LOGIN_SUCCESS, data: data });
    } else {
      yield put({ type: ActionTypes.LOGIN_FAILED, message: message });
    }
  } catch (error) {
    yield put({ type: ActionTypes.LOGIN_FAILED, message: error });
  }
}

function* loginFacebook(payload) {
  try {
    setHeaderToken(null);
    const { status, data, message } = ServiceUtility.processResponseService(
      yield call(
        LoginService.loginFacebookFunction,
        JSON.stringify(payload.params)
      )
    );
    if (status) {
      yield put({ type: ActionTypes.LOGIN_FACEBOOK_SUCCESS, data: data });
    } else {
      yield put({ type: ActionTypes.LOGIN_FACEBOOK_FAILED, message: message });
    }
  } catch (error) {
    yield put({ type: ActionTypes.LOGIN_FACEBOOK_FAILED, message: error });
  }
}

function* loginGoogle(payload) {
  try {
    setHeaderToken(null);
    const { status, data, message } = ServiceUtility.processResponseService(
      yield call(
        LoginService.loginGoogleFunction,
        JSON.stringify(payload.params)
      )
    );
    if (status) {
      yield put({ type: ActionTypes.LOGIN_GOOGLE_SUCCESS, data: data });
    } else {
      yield put({ type: ActionTypes.LOGIN_GOOGLE_FAILED, message: message });
    }
  } catch (error) {
    yield put({ type: ActionTypes.LOGIN_GOOGLE_FAILED, message: error });
  }
}

function* logout() {
  try {    
    yield put({ type: ActionTypes.LOGOUT_SUCCESS });
  } catch (error) {
    yield put({ type: ActionTypes.LOGOUT_FAILED, message: error });
  }
}

function* loginSagas() {
  yield takeLatest(ActionTypes.LOGIN_REQUEST, login);
  yield takeLatest(ActionTypes.LOGIN_FACEBOOK_REQUEST, loginFacebook);
  yield takeLatest(ActionTypes.LOGIN_GOOGLE_REQUEST, loginGoogle);
  yield takeLatest(ActionTypes.LOGOUT_REQUEST, logout);
}

// export
export default loginSagas;
