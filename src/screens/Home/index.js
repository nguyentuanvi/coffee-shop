// @flow
import React, { Component } from 'react';
import { View, Text, ImageBackground, Image } from 'react-native';

import { connect } from 'react-redux';
import {
  Utils,
  Images,
  I18n,
  Validators,
  AppStyles,
  Logger
} from '../../common';
import AbstractComponent from '../../components/AbstractComponent';
import TouchableEffect from '../../components/Touchable';
import AppHeader from '../../components/AppHeader';
import Container from '../../components/Container';
import Content from '../../components/Content';

import styles from './styles';
import AppText from '../../components/AppText';

class Home extends AbstractComponent {
  render() {
    return (
      <Container>
        <Content>
          <AppHeader
            navigation={this.props.navigation}
            drawer
            title={I18n.t('login.phoneLoginNavTitle')}
          />
          <AppText text={'Home screen'} />
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    loginReducer: state.loginReducer
  };
}

Home = connect(mapStateToProps)(Home);

export default Home;
