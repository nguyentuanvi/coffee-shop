// @flow
import React, { Component } from 'react';
import { StackActions, NavigationActions } from 'react-navigation';
import {
  Image,
  ImageBackground,
  Platform,
  StatusBar,
  Dimensions,
  View,
  NativeModules
} from 'react-native';
import { connect } from 'react-redux';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import firebase from 'react-native-firebase';
import { GoogleSignin } from 'react-native-google-signin';

import AbstractComponent from '../../components/AbstractComponent';
import Loading from '../../components/Loading';
import TouchableEffect from '../../components/Touchable';
import InputValidator from '../../components/InputWithVilidator';
import AppText from '../../components/AppText';
import Container from '../../components/Container';
import Content from '../../components/Content';

import {
  I18n,
  Images,
  Colors,
  AppStyles,
  Utils,
  ActionTypes,
  Logger,
  Validators,
  FormUtils
} from '../../common';

import styles from './styles';

import * as LoginActions from '../../actions/loginAction';

const Zalo = NativeModules.Zalo;

const { deviceHeight, deviceWidth } = Dimensions.get('window');

const iosClientId =
  '472459180917-60n6rjadsj9r0pstaqcsnqqlng0t8gol.apps.googleusercontent.com';

// import commonColor from "../../theme/variables/commonColor";

class LoginForm extends AbstractComponent {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      emailValid: false,
      passValid: false
    };

    this.crashlytic = firebase.crashlytics();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    // No state update necessary
    return null;
  }

  componentDidUpdate(prevProps, prevState) {
    const { loginReducer } = this.props;

    if (prevProps.loginReducer.actionType !== loginReducer.actionType) {
      const { actionType } = loginReducer;
      if (
        actionType === ActionTypes.LOGIN_SUCCESS ||
        actionType === ActionTypes.LOGIN_FACEBOOK_SUCCESS ||
        actionType === ActionTypes.LOGIN_GOOGLE_SUCCESS
      ) {
        Utils.showSuccessAlert('Login successfully!');
        this.navigateToApp();
        return;
      }

      if (
        actionType === ActionTypes.LOGIN_FAILED ||
        actionType === ActionTypes.LOGIN_FACEBOOK_FAILED ||
        actionType === ActionTypes.LOGIN_GOOGLE_FAILED
      ) {
        let errorMsg =
          loginReducer.auth && loginReducer.auth.message
            ? loginReducer.auth.message
            : '';
        Utils.showErrorAlert(errorMsg);
        return;
      }
      return;
    }
  }

  navigateToApp() {
    return this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Drawer' })]
      })
    );
  }

  showLoginPhone() {
    this.props.navigation.navigate('PhoneLogin');
  }

  async googleLogin(usingFirebase = false) {
    try {
      // Add any configuration settings here:
      let config = {};
      if (Platform.OS === 'ios') {
        config['iosClientId'] = iosClientId;
      } else if (Platform.OS === 'android') {
        GoogleSignin.hasPlayServices({ autoResolve: true });
      }
      await GoogleSignin.configure(config);

      const data = await GoogleSignin.signIn();

      const params = {
        id_token: data.idToken
      };

      Logger.log('Google signin: ', data);
      this.props.dispatch(LoginActions.loginGoogle(params));

      if (usingFirebase) {
        // create a new firebase credential with the token
        const credential = firebase.auth.GoogleAuthProvider.credential(
          data.idToken,
          data.accessToken
        );
        // login with credential
        const currentUser = await firebase
          .auth()
          .signInAndRetrieveDataWithCredential(credential);

        Logger.log(JSON.stringify(currentUser.user.toJSON()));
      }
    } catch (e) {
      Logger.log(e);
      Utils.showErrorAlert(e.toString());
    }
  }

  async facebookLogin(usingFirebase = false) {
    try {
      const result = await LoginManager.logInWithReadPermissions([
        'public_profile',
        'email'
      ]);

      if (result.isCancelled) {
        Logger.log('User cancelled request');
        Utils.showErrorAlert('User cancelled request');
        return;
      }

      Logger.log(
        `Login success with permissions: ${result.grantedPermissions.toString()}`
      );

      // get the access token
      const data = await AccessToken.getCurrentAccessToken();

      if (!data) {
        Logger.log('Something went wrong obtaining the users access token');
        return;
      }

      Logger.log('Facebook access token: ', data);

      const params = {
        access_token: data.accessToken
      };

      this.props.dispatch(LoginActions.loginFacebook(params));

      if (usingFirebase) {
        // create a new firebase credential with the token
        const credential = firebase.auth.FacebookAuthProvider.credential(
          data.accessToken
        );

        // login with credential
        const currentUser = await firebase
          .auth()
          .signInAndRetrieveDataWithCredential(credential);

        Logger.info(JSON.stringify(currentUser.user.toJSON()));
      }
    } catch (e) {
      Logger.log(e);
      Utils.showErrorAlert(e);
    }
  }

  async login() {
    const { emailValid, passValid } = this.state;
    let isValid = emailValid && passValid;
    if (isValid) {
      const params = {
        email: this.state.email,
        password: this.state.password
      };

      this.props.dispatch(LoginActions.login(params));
    } else {
      Utils.showErrorAlert('Enter Valid Username & password!');
    }
  }

  loginZalo() {
    Zalo.login()
      .then(resp => {
        Logger.log('Zalo resp: ', resp);
      })
      .catch(err => {
        Logger.log('Zalo error: ', err);
      });
  }

  renderLoginButton(text, onPress = () => {}, disabled = false) {
    return (
      <TouchableEffect
        block
        large
        style={[styles.loginBtn]}
        onPress={() => onPress()}
        disabled={disabled}
        title={text}
      >
        <AppText text={text} />
      </TouchableEffect>
    );
  }

  renderLoginButtons() {
    const { emailValid, passValid } = this.state;
    let isValid = emailValid && passValid;
    return (
      <View style={styles.formContainer}>
        {this.renderLoginButton(
          I18n.t('login.getStarted'),
          () => this.login(),
          !isValid
        )}
        {this.renderLoginButton(I18n.t('login.loginFb'), () =>
          this.facebookLogin(false)
        )}
        {this.renderLoginButton(I18n.t('login.loginGg'), () =>
          this.googleLogin(false)
        )}
        {this.renderLoginButton(I18n.t('login.loginPhone'), () =>
          this.showLoginPhone()
        )}
        {this.renderLoginButton(I18n.t('login.zaloLogin'), () =>
          this.loginZalo()
        )}
      </View>
    );
  }

  // renderSignupAndForgot() {
  //   const navigation = this.props.navigation;
  //   return (
  //     <View style={styles.otherLinksContainer}>
  //       <Left>
  //         <TouchableEffect
  //           small
  //           transparent
  //           style={{ alignSelf: 'flex-start' }}
  //           onPress={() => navigation.navigate('SignUp')}
  //         >
  //           <Text style={styles.helpBtns}>{I18n.t('login.createAccount')}</Text>
  //         </TouchableEffect>
  //       </Left>
  //       <Right>
  //         <TouchableEffect
  //           small
  //           transparent
  //           style={{ alignSelf: 'flex-end' }}
  //           onPress={() => navigation.navigate('ForgotPassword')}
  //         >
  //           <Text style={styles.helpBtns}>{I18n.t('login.forgotPass')}</Text>
  //         </TouchableEffect>
  //       </Right>
  //     </View>
  //   );
  // }

  render() {
    return (
      <Container>
        <StatusBar barStyle="light-content" />
        <ImageBackground source={Images.background} style={styles.background}>
          <Content contentContainerStyle={styles.contentContainer}>
            <View style={styles.logoContainer}>
              <Image source={Images.logo} style={styles.logo} />
            </View>
            <View style={styles.container}>
              <View style={styles.form}>
                <InputValidator
                  placeholder={I18n.t('login.emailLabel')}
                  input={{ keyboardType: 'email-address' }}
                  containerStyle={{ backgroundColor: 'rgba(255,255,255,0.3)' }}
                  icon="person"
                  value={this.state.email}
                  validators={[Validators.email, Validators.required]}
                  onChangeText={text => {
                    this.setState({
                      email: text
                    });
                  }}
                  onValidatationChanged={valid => {
                    this.setState({
                      emailValid: valid
                    });
                  }}
                />
                <InputValidator
                  placeholder={I18n.t('login.passLabel')}
                  input={{}}
                  containerStyle={{ backgroundColor: 'rgba(255,255,255,0.3)' }}
                  secureTextEntry={true}
                  icon="lock"
                  value={this.state.password}
                  validators={[Validators.minLength8, Validators.required]}
                  onChangeText={text => {
                    this.setState({
                      password: text
                    });
                  }}
                  onValidatationChanged={valid => {
                    this.setState({
                      passValid: valid
                    });
                  }}
                />

                {this.renderLoginButtons()}

                {/* {this.renderSignupAndForgot()} */}
              </View>
            </View>
          </Content>
        </ImageBackground>
        <Loading visible={this.props.loginReducer.auth.isLoading} />
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    loginReducer: state.loginReducer
  };
}

LoginForm = connect(mapStateToProps)(LoginForm);

export default LoginForm;
