import React, { Component } from 'react';
import {} from 'react-native';

import { Utils, Logger, I18n, Colors, Consts } from '../../common';
import AbstractComponent from '../../components/AbstractComponent';
import AppHeader from '../../components/AppHeader';
import AppText from '../../components/AppText';
import TouchableEffect from '../../components/Touchable';
import AppImage from '../../components/AppImage';
import Container from '../../components/Container';
import Content from '../../components/Content';
import styles from './styles';

const images = [
  'https://trafodion.apache.org/images/logos/trafodion-dragon-large.png',
  'https://upload.wikimedia.org/wikipedia/commons/9/96/Druplicon.large.png',
  'https://i.imgflip.com/1zbjuq.jpg',
  'https://images.unsplash.com/photo-1444011283387-7b0f76371f12?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=cbc3b29b62775efc573115ee5f413905&w=1000&q=80',
  'https://images-cdn.9gag.com/photo/aBwWKyZ_700b.jpg',
  'https://s1.r29static.com//bin/entry/c01/720x1080,80/1820116/image.jpg',
  'https://s3.r29static.com//bin/entry/af6/340x408,80/1941131/image.jpg',
  'https://i.pinimg.com/736x/36/f0/c5/36f0c5e5ec18402f8d788305cc96dc5d--megan-fox-images-foxes.jpg'
];

export default class ImageScreen extends AbstractComponent {
  renderImage(uri, index) {
    return <AppImage key={index} source={{ uri: uri }} style={styles.image} />;
  }

  render() {
    return (
      <Container>
        <AppHeader navigation={this.props.navigation} drawer title={I18n.t('apptitle.image')} />
        <Content padder>
          {images.map((item, index, arr) => {
            return this.renderImage(item, index);
          })}
        </Content>
      </Container>
    );
  }
}
