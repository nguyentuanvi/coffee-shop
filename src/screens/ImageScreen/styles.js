import { StyleSheet } from 'react-native';

const styles = {
  image: {
    width: 300,
    height: 300,
    borderRadius: 150
  }
};

export default styles;
