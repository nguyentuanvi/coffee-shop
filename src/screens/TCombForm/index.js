import React, { Component } from 'react';
import {} from 'react-native';
import moment from 'moment';
import t from 'tcomb-form-native';

import { Utils, Logger, I18n, Colors, Consts } from '../../common';
import AbstractComponent from '../../components/AbstractComponent';
import AppHeader from '../../components/AppHeader';
import AppText from '../../components/AppText';
import TouchableEffect from '../../components/Touchable';
import Container from '../../components/Container';
import Content from '../../components/Content';
import styles from './styles';

let Form = t.form.Form;

// a type representing positive numbers
let Positive = t.refinement(t.Number, n => {
  return n >= 0;
});

let Email = t.refinement(t.String, s => {
  return Utils.validateEmail(s);
});

let Gender = t.enums({
  M: 'Male',
  F: 'Female'
});

let Books = t.struct({
  name: t.String,
  description: t.maybe(t.String)
});

// here we are: define your domain model
let Person = t.struct({
  name: t.String, // a required string
  surname: t.maybe(t.String), // an optional string,
  email: Email,
  age: Positive, // a required number
  gender: Gender,
  birthDate: t.Date, // a date field
  rememberMe: t.Boolean, // a boolean,
  interestedBooks: t.list(Books)
});

let defaultOptions = {
  fields: {
    name: {
      error: 'Required',
      placeholder: 'Your placeholder here',
      label: 'Insert your name',
      help: 'Your help message here'
    },
    age: {
      error: 'Required'
    },
    birthDate: {
      config: {
        format: date => {
          return moment(date).format(Consts.FORMAT_DAY_MONTH_DATE);
        }
      },
      mode: 'date'
    }
  }
  //   order: ['name', 'surname', 'rememberMe', 'gender', 'age', 'email', 'birthDate']
}; // optional rendering options (see documentation)

export default class TForm extends AbstractComponent {
  constructor(props) {
    super(props);
    this.state = {
      value: {},
      options: defaultOptions
    };
  }

  onPress() {
    // call getValue() to get the values of the form
    let value = this.form.getValue();
    if (value) {
      // if validation fails, value will be null
      Logger.log(value); // value here is an instance of Person

      setTimeout(() => {
        let options = t.update(this.state.options, {
          fields: {
            name: {
              hasError: { $set: true },
              error: { $set: 'Name is exists' }
            }
          }
        });

        this.setState({
          options: options
        });
      }, 2000);
    }
  }

  formChange(value) {
    this.setState({ value });
  }

  render() {
    return (
      <Container>
        <AppHeader navigation={this.props.navigation} drawer title={I18n.t('apptitle.tform')} />
        <Content padder>
          <Form
            ref={ref => {
              this.form = ref;
            }}
            type={Person}
            value={this.state.value}
            options={this.state.options}
            onChange={value => this.formChange(value)}
          />
          <TouchableEffect block large onPress={() => this.onPress()}>
            <AppText text={'Save'} />
          </TouchableEffect>
        </Content>
      </Container>
    );
  }
}
