import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';

import { Utils, Logger, I18n } from '../../common';

import AbstractComponent from '../../components/AbstractComponent';
import AppHeader from '../../components/AppHeader';
import Container from '../../components/Container';
import Content from '../../components/Content';

import styles from './styles';

export default class MyCalendarList extends AbstractComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container>
        <AppHeader
          navigation={this.props.navigation}
          drawer
          title={I18n.t('apptitle.calendarList')}
        />
        <CalendarList
          current={new Date()}
          pastScrollRange={24}
          futureScrollRange={24}
        />
      </Container>
    );
  }
}
