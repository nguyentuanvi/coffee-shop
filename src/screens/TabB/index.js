import React, { Component } from 'react';
import { Dimensions, View } from 'react-native';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import moment from 'moment';
import DateTimePicker from 'react-native-modal-datetime-picker';

import { Utils, Images, I18n, Validators, AppStyles, Logger, Colors, Consts } from '../../common';
import styles from './styles';
import AbstractComponent from '../../components/AbstractComponent';
import AppHeader from '../../components/AppHeader';
import Container from '../../components/Container';
import Content from '../../components/Content';
import InputValidator from '../../components/InputWithVilidator';
import AppText from '../../components/AppText';
import TouchableEffect from '../../components/Touchable';

class TabB extends AbstractComponent {
  static navigationOptions = ({ navigation }) => ({
    title: 'Tab B'
  });

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      visible: false,
      date: new Date()
    };
  }

  _showDateTimePicker() {
    this.setState({ visible: true });
  }

  _hideDateTimePicker() {
    this.setState({ visible: false });
  }

  _handleDatePicked(date) {
    this.setState({
      date
    });
    this._hideDateTimePicker();
  }

  formatDate(date) {
    let m = moment(date);
    return m.format(Consts.FORMAT_APPOINTMENT_DATE);
  }

  render() {
    return (
      <Container>
        <AppHeader navigation={this.props.navigation} drawer title={'Tab B'} />
        <Content padder contentContainerStyle={styles.container}>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <AppText style={{ alignSelf: 'center' }} text={this.formatDate(this.state.date)} />
            <TouchableEffect block large style={{ marginTop: 10 }} onPress={() => this._showDateTimePicker()}>
              <AppText text={I18n.t('tab2.showPicker')} />
            </TouchableEffect>
            <DateTimePicker
              isVisible={this.state.visible}
              onConfirm={date => this._handleDatePicked(date)}
              onCancel={() => this._hideDateTimePicker()}
              date={this.state.date}
            />
          </View>
        </Content>
      </Container>
    );
  }
}
export default TabB;
