import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text: {
    color: 'white',
    fontSize: 40,
    fontWeight: 'bold'
  },
  form: {
    flex: 1,
  },
});

export default styles;
