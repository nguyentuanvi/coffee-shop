import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  instructions: {
    marginTop: 20,
    marginBottom: 20
  },
  contentContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  button: {        
    marginBottom: 10
  }
});

export default styles;
