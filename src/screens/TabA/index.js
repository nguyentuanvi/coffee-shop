import React, { Component } from 'react';
import { View, TextInput, Platform } from 'react-native';
import Modal from 'react-native-modalbox';

import { Utils, Images, I18n, Validators, AppStyles, Logger, Colors } from '../../common';

import styles from './styles';
import AbstractComponent from '../../components/AbstractComponent';
import AppHeader from '../../components/AppHeader';
import AppText from '../../components/AppText';
import Container from '../../components/Container';
import Content from '../../components/Content';
import TouchableEffect from '../../components/Touchable';

class TabA extends AbstractComponent {
  static navigationOptions = ({ navigation }) => ({
    title: 'Tab A'
  });

  render() {
    return (
      <Container>
        <AppHeader
          navigation={this.props.navigation}
          drawer
          title={'Tab A'}
          showRightButton={true}
          rightIcon={'ios-camera'}
          onRightPressed={() => {
            this.props.navigation.navigate('Camera');
          }}
        />
        <Content padder contentContainerStyle={styles.container}>
          <TouchableEffect
            primary
            block
            large
            onPress={() => {
              this.modal.open();
            }}
          >
            <AppText text={'Open modal'} />
          </TouchableEffect>
        </Content>
        <Modal
          style={[styles.modal, styles.modal3]}
          position={'center'}
          ref={ref => {
            this.modal = ref;
          }}
          swipeToClose={true}
        >
          <AppText text={'Modal centered'} />
        </Modal>
      </Container>
    );
  }
}
export default TabA;
