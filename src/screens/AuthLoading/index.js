import React, { Component } from 'react';
import {
  ImageBackground,
  ActivityIndicator,
  StyleSheet,
  Dimensions
} from 'react-native';
import { Storage, Consts, Logger } from '../../common';
import AbstractComponent from '../../components/AbstractComponent';
import Container from '../../components/Container';

import { background } from '../../../assets';

import { setHeaderToken } from '../../services/apis/commonApi';

const { width, height } = Dimensions.get('window');

export default class AuthLoading extends AbstractComponent {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userInfo = await Storage.getDataJson(Consts.USER);

    if (userInfo) {
      setHeaderToken(userInfo);
    }

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(userInfo ? 'App' : 'Auth');
  };

  // Render any loading content that you like here
  render() {
    return (
      <Container>
        <ImageBackground style={styles.image} source={background}>
          <ActivityIndicator animated={true} />
        </ImageBackground>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width,
    height,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
