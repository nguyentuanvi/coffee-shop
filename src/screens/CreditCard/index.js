import React, { Component } from 'react';
import { View } from 'react-native';

import { CreditCardInput } from 'react-native-credit-card-input';

import {
  Utils,
  Images,
  I18n,
  Validators,
  AppStyles,
  Logger,
  Colors
} from '../../common';

import AbstractComponent from '../../components/AbstractComponent';
import AppHeader from '../../components/AppHeader';
import Container from '../../components/Container';
import styles from './styles';

export default class CreditCard extends AbstractComponent {
  _onChange(formData) {
    Logger.log(formData);
  }

  render() {
    return (
      <Container>
        <AppHeader
          navigation={this.props.navigation}
          drawer
          title={I18n.t('apptitle.credit')}
        />
        <CreditCardInput
          autoFocus
          requiresName
          requiresCVC
          labelStyle={styles.label}
          inputStyle={styles.input}
          validColor={Colors.COLOR_BLACK}
          invalidColor={Colors.COLOR_RED}
          placeholderColor={Colors.COLOR_DISABLE}
          onChange={formData => this._onChange(formData)}
        />
      </Container>
    );
  }
}
