import { StyleSheet } from 'react-native';
import { Consts, Dimens } from '../../common';

const styles = StyleSheet.create({
   label: {
    color: "black",
    fontSize: Dimens.FONT_DESCRIPTION,
  },
  input: {
    fontSize: Dimens.FONT_CONTENT,
    color: "black",
  }
});

export default styles;
