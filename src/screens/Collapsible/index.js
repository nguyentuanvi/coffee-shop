import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, FlatList } from 'react-native';
import * as Animatable from 'react-native-animatable';
import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';

import { Utils, Logger, I18n, Colors, Consts } from '../../common';
import AbstractComponent from '../../components/AbstractComponent';
import AppHeader from '../../components/AppHeader';
import AppText from '../../components/AppText';
import TouchableEffect from '../../components/Touchable';
import Container from '../../components/Container';
import Content from '../../components/Content';
import styles from './styles';

const BACON_IPSUM =
  'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs. Picanha beef prosciutto meatball turkey shoulder shank salami cupim doner jowl pork belly cow. Chicken shankle rump swine tail frankfurter meatloaf ground round flank ham hock tongue shank andouille boudin brisket. ';

const CONTENT = [
  {
    title: 'First',
    content: BACON_IPSUM
  },
  {
    title: 'Second',
    content: BACON_IPSUM
  },
  {
    title: 'Third',
    content: BACON_IPSUM
  },
  {
    title: 'Fourth',
    content: BACON_IPSUM
  },
  {
    title: 'Fifth',
    content: BACON_IPSUM
  }
];

const SELECTORS = [
  {
    title: 'First',
    value: 0
  },
  {
    title: 'Third',
    value: 2
  },
  {
    title: 'None',
    value: false
  }
];

const ITEMS = [
  {
    key: 1,
    title: 'Collapsible 1',
    content: 'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs'
  },
  {
    key: 2,
    title: 'Collapsible 2',
    content: 'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs'
  },
  {
    key: 3,
    title: 'Collapsible 3',
    content: 'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs'
  },
  {
    key: 4,
    title: 'Collapsible 4',
    content: 'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs'
  },
  {
    key: 5,
    title: 'Collapsible 5',
    content: 'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs'
  },
  {
    key: 6,
    title: 'Collapsible 6',
    content: 'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs'
  },
  {
    key: 7,
    title: 'Collapsible 7',
    content: 'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs'
  },
  {
    key: 8,
    title: 'Collapsible 8',
    content: 'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs'
  },
  {
    key: 9,
    title: 'Collapsible 9',
    content: 'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs'
  },
  {
    key: 10,
    title: 'Collapsible 7',
    content: 'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs'
  },
  {
    key: 11,
    title: 'Collapsible 8',
    content: 'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs'
  },
  {
    key: 12,
    title: 'Collapsible 9',
    content: 'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs'
  },
  {
    key: 13,
    title: 'Collapsible 7',
    content:
      'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs'
  },
  {
    key: 14,
    title: 'Collapsible 8',
    content:
      'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs'
  },
  {
    key: 15,
    title: 'Collapsible 9',
    content:
      'Bacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribsBacon ipsum dolor amet chuck turducken landjaeger tongue spare ribs'
  }
];

export default class CollapsibleScreen extends AbstractComponent {
  state = {
    activeSection: false,
    collapsed: true
  };

  constructor(props) {
    super(props);
    this.state = {
      appears: {}
    };
  }

  _toggleExpanded(item) {
    let appears = { ...this.state.appears };
    appears[item.key] = !appears[item.key];
    this.setState({ appears: appears }, () => {
      console.log(this.state.appears);
    });
  }

  _setSection(section) {
    this.setState({ activeSection: section });
  }

  _renderHeader(section, i, isActive) {
    return (
      <Animatable.View
        duration={400}
        style={[styles.header, isActive ? styles.active : styles.inactive]}
        transition="backgroundColor"
      >
        <Text style={styles.headerText}>{section.title}</Text>
      </Animatable.View>
    );
  }

  _renderContent(section, i, isActive) {
    return (
      <Animatable.View
        duration={400}
        style={[styles.content, isActive ? styles.active : styles.inactive]}
        transition="backgroundColor"
      >
        <Animatable.Text animation={isActive ? 'bounceIn' : undefined}>{section.content}</Animatable.Text>
      </Animatable.View>
    );
  }

  renderItem(item) {
    return (
      <View style={{ flex: 1 }}>
        <TouchableOpacity onPress={() => this._toggleExpanded(item)}>
          <View style={styles.header}>
            <Text style={styles.headerText}>{item.title}</Text>
          </View>
        </TouchableOpacity>
        <Collapsible collapsed={!this.state.appears[item.key]} align="center">
          <View style={styles.content}>
            <Text>{item.content}</Text>
          </View>
        </Collapsible>
      </View>
    );
  }

  render() {
    return (
      <Container>
        <AppHeader navigation={this.props.navigation} drawer title={I18n.t('apptitle.collapsible')} />
        <Content padder>
          <Text style={styles.title}>Accordion Example</Text>
          <FlatList
            style={{ flex: 1 }}
            keyExtractor={item => {
              return item.key.toString();
            }}
            data={ITEMS}
            renderItem={({ item }) => this.renderItem(item)}
            extraData={this.state.collapses}
          />
        </Content>
      </Container>
    );
  }
}
