import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  map: {
    flex: 1
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20
  },
  latlng: {
    width: 200,
    alignItems: 'stretch'
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10
  },
  buttonContainer: {
    width: '100%',
    flexDirection: 'row',
    marginVertical: 20,    
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default styles;
