import React, { Component } from 'react';
import { View, Geolocation } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE, Polyline } from 'react-native-maps';

import { Utils, Logger, I18n, Colors, Consts, PERMISSIONS, PermissionUtils } from '../../common';
import AbstractComponent from '../../components/AbstractComponent';
import AppHeader from '../../components/AppHeader';
import AppText from '../../components/AppText';
import TouchableEffect from '../../components/Touchable';
import AppImage from '../../components/AppImage';
import Container from '../../components/Container';
import Content from '../../components/Content';
import styles from './styles';

let id = 0;
const initRegion = {
  latitude: 37.78825,
  longitude: -122.4324,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421
};

function randomColor() {
  return `#${Math.floor(Math.random() * 16777215).toString(16)}`;
}

export default class Maps extends AbstractComponent {
  constructor(props) {
    super(props);
    this.state = {
      markers: []
    };
  }

  componentDidMount() {
    setTimeout(() => {
      PermissionUtils.checkAndRequest(PERMISSIONS.Location)
        .then(authorized => {
          Logger.log(authorized);
          if (authorized) {
            navigator.geolocation.getCurrentPosition(
              position => {
                Logger.log('Current location: ', position);

                this.map.animateToCoordinate(
                  {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                  },
                  1000
                );

                this.setState({
                  markers: [
                    ...this.state.markers,
                    {
                      coordinate: position.coords,
                      key: id++,
                      color: randomColor()
                    }
                  ]
                });
              },
              error => {
                Logger.log('Get location error: ', error);
              },
              {
                enableHighAccuracy: false,
                timeout: 10000,
                maxAge: 1000
              }
            );
          }
        })
        .catch(err => {
          Logger.log(err);
        });
    }, 5000);
  }

  onRegionChange(region) {
    // console.log(region);
    this.setState({ region });
  }

  onMapPress(e) {
    this.setState({
      markers: [
        ...this.state.markers,
        {
          coordinate: e.nativeEvent.coordinate,
          key: id++,
          color: randomColor()
        }
      ]
    });
  }

  render() {
    return (
      <Container>
        <AppHeader navigation={this.props.navigation} drawer title={I18n.t('apptitle.maps')} />
        <MapView
          ref={ref => {
            this.map = ref;
          }}
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          initialRegion={initRegion}
          onRegionChangeComplete={region => this.onRegionChange(region)}
          onPress={e => this.onMapPress(e)}
        >
          {this.state.markers.map(marker => (
            <Marker key={marker.key} coordinate={marker.coordinate} pinColor={marker.color} draggable />
          ))}
        </MapView>
        <View style={styles.buttonContainer}>
          <TouchableEffect onPress={() => this.setState({ markers: [] })} style={styles.bubble}>
            <AppText text={'Tap to create a marker of random color'} />
          </TouchableEffect>
        </View>
      </Container>
    );
  }
}
