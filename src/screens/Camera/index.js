import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { RNCamera, FaceDetector } from 'react-native-camera';
import { connect } from 'react-redux';

import { googleVisionRequest } from '../../actions/googlevisionAction';
import AbstractComponent from '../../components/AbstractComponent';
import AppHeader from '../../components/AppHeader';
import AppText from '../../components/AppText';
import TouchableEffect from '../../components/Touchable';
import { Utils, Logger } from '../../common';
import styles from './styles';

class Camera extends AbstractComponent {
  constructor(props) {
    super(props);
    this.state = {
      cameraReady: false
    };
  }

  renderPendingView() {
    return (
      <View style={styles.pendingView}>
        <AppText text={'Waiting'} />
      </View>
    );
  }

  renderNotAuthorizedView() {
    return (
      <View style={styles.notAuthorizedView}>
        <AppText text={'Not authorized'} style={styles.notAuthText} />
      </View>
    );
  }

  render() {
    const { cameraReady } = this.state;
    return (
      <View style={styles.container}>
        <AppHeader
          navigation={this.props.navigation}
          title={'Camera'}
          leftText={'Home'}
        />
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          autoFocus={RNCamera.Constants.AutoFocus.on}
          flashMode={RNCamera.Constants.FlashMode.off}
          permissionDialogTitle={'Permission to use camera'}
          permissionDialogMessage={
            'We need your permission to use your camera phone'
          }
          pendingAuthorizationView={this.renderPendingView()}
          notAuthorizedView={this.renderNotAuthorizedView()}
          onCameraReady={() => {
            this.setState({
              cameraReady: true
            });
          }}
          onBarCodeRead={data => {
            Logger.log('Barcode: ', data);
          }}
        />
        {cameraReady && (
          <View style={styles.contentContainer}>
            <TouchableEffect
              onPress={() => this.takePicture()}
              style={styles.capture}
            >
              <AppText text={'SNAP'} />
            </TouchableEffect>
          </View>
        )}
      </View>
    );
  }

  async takePicture() {
    if (this.camera) {
      const options = { quality: 1, base64: true };
      try {
        const data = await this.camera.takePictureAsync(options);
        // console.log(data);
        this.props.dispatch(googleVisionRequest(data.base64));
      } catch (err) {
        Utils.showErrorAlert(err);
      }
    }
  }
}

function mapStateToProps(state) {
  return {
    googleVisionReducer: state.googleVisionReducer
  };
}

Camera = connect(mapStateToProps)(Camera);

export default Camera;
