// @flow
import React, { Component } from 'react';
import { View, Text, ImageBackground, Image } from 'react-native';

import { connect } from 'react-redux';

import firebase from 'react-native-firebase';

import { StackActions, NavigationActions } from 'react-navigation';

import { Utils, Images, I18n, Validators, AppStyles, Logger, Colors } from '../../common';

import AbstractComponent from '../../components/AbstractComponent';
import InputValidator from '../../components/InputWithVilidator';
import TouchableEffect from '../../components/Touchable';
import AppText from '../../components/AppText';
import AppHeader from '../../components/AppHeader';
import Container from '../../components/Container';
import Content from '../../components/Content';

import styles from './styles';

const INPUT_PHONE = 'input_phone';
const SENDING_CODE = 'sending_code';
const INPUT_CODE = 'input_code';
const VERIFYING_CODE = 'verifying';

class PhoneLogin extends AbstractComponent {
  constructor(props) {
    super(props);
    this.state = {
      sendCodeBtn: I18n.t('login.sendCode'),
      verifyCodeBtn: I18n.t('login.verifyCode'),
      step: INPUT_PHONE,
      loading: false,
      phoneNo: '',
      codeInput: ''
    };

  }

  navigateToWalkthrough() {
    return this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Walkthrough' })]
      })
    );
  }

  sendCode() {
    this.setState({
      sendCodeBtn: I18n.t('login.sendingCode'),
      loading: true
    });
    firebase
      .auth()
      .signInWithPhoneNumber(this.state.phoneNo)
      .then(confirmResult => {
        this.confirmResult = confirmResult;
        this.setState({
          sendCodeBtn: I18n.t('login.sendCode'),
          loading: false,
          step: INPUT_CODE
        });
      })
      .catch(error => {
        this.setState({
          sendCodeBtn: I18n.t('login.sendCode'),
          loading: false
        });
        Logger.log(error);
        Utils.showErrorAlert(error.toString());
      });
  }

  verifyCode() {
    if (this.confirmResult) {
      this.setState({
        verifyCodeBtn: I18n.t('login.verifyingCode'),
        loading: true
      });
      this.confirmResult
        .confirm(this.state.codeInput)
        .then(user => {
          Logger.log('Login by phone: ', user);
          this.navigateToWalkthrough();
          // this.setState({
          //   verifyCodeBtn: I18n.t('login.verifyCode'),
          //   loading: false
          // });
        })
        .catch(error => {
          this.setState({
            verifyCodeBtn: I18n.t('login.verifyCode'),
            loading: false
          });
          Logger.log(error);
          Utils.showErrorAlert(error.toString());
        });
    }
  }

  renderPhoneInput() {
    return (
      <View style={styles.form}>
        <InputValidator
          placeholder={I18n.t('login.phoneLabel')}
          input={{ keyboardType: 'phone-pad' }}
          icon="call"
          value={this.state.phoneNo}
          validators={[Validators.required, Validators.phoneNumber]}
          onChangeText={text => {
            this.setState({
              phoneNo: text
            });
          }}
          onValidatationChanged={valid => {
            this.setState({
              phoneValid: valid
            });
          }}
        />

        <TouchableEffect
          primary
          block
          large
          disabled={this.state.loading || !this.state.phoneValid}
          style={styles.loginBtn}
          onPress={() => this.sendCode()}
        >
          <AppText text={this.state.sendCodeBtn} />
        </TouchableEffect>
      </View>
    );
  }

  renderCodeInput() {
    return (
      <View style={styles.form}>
        <InputValidator
          placeholder={I18n.t('login.codeLabel')}
          input={{ keyboardType: 'phone-pad' }}
          icon="code"
          value={this.state.codeInput}
          validators={[Validators.required]}
          onChangeText={text => {
            this.setState({
              codeInput: text
            });
          }}
          onValidatationChanged={valid => {
            this.setState({
              codeValid: valid
            });
          }}
        />

        <TouchableEffect
          primary
          block
          large
          disabled={this.state.loading || !this.state.codeValid}
          style={styles.loginBtn}
          onPress={() => this.verifyCode()}
        >
          <AppText text={this.state.verifyCodeBtn} />
        </TouchableEffect>
      </View>
    );
  }

  render() {
    return (
      <Container>
        <ImageBackground source={Images.background} style={styles.background}>
          <AppHeader
            navigation={this.props.navigation}
            title={I18n.t('login.phoneLoginNavTitle')}
            headerStyle={{ backgroundColor: Colors.COLOR_PRIMARY }}
          />
          <Content contentContainerStyle={{ flex: 1 }} keyboardShouldPersistTaps={'never'}>
            <View style={styles.logoContainer}>
              <Image source={Images.logo} style={styles.logo} />
            </View>
            <View style={styles.container}>
              {this.state.step === INPUT_PHONE ? this.renderPhoneInput() : this.renderCodeInput()}
            </View>
          </Content>
        </ImageBackground>
      </Container>
    );
  }
}

PhoneLogin.defaultProps = {
  onLeftPressed: () => {}
};

function mapStateToProps(state) {
  return {
    loginReducer: state.loginReducer
  };
}

PhoneLogin = connect(mapStateToProps)(PhoneLogin);

export default PhoneLogin;
