import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import RNFetchBlob from 'react-native-fetch-blob';

import styles from './styles';
import AbstractComponent from '../../components/AbstractComponent';
import AppHeader from '../../components/AppHeader';
import AppText from '../../components/AppText';
import TouchableEffect from '../../components/Touchable';
import Container from '../../components/Container';
import Content from '../../components/Content';
import { Utils, Logger, Consts } from '../../common';
import { apiUploadFile, wrapFileData } from '../../services/apis/commonApi';
import * as API_URLS from '../../services/configs';

class TabC extends AbstractComponent {
  static navigationOptions = ({ navigation }) => ({
    title: 'Tab C'
  });

  render() {
    return (
      <Container>
        <AppHeader navigation={this.props.navigation} drawer title={'Tab B'} />
        <Content contentContainerStyle={styles.container}>
          <TouchableEffect
            primary
            block
            large
            onPress={() => {
              Utils.selectPhoto(({ result, success, errorMsg }) => {
                if (success) {
                  Logger.log('Image uri: ', result);
                  apiUploadFile('POST', API_URLS.BASE_URL + '/api/users/update_avatar', [
                    {
                      name: 'avatar',
                      filename: result.fileName,
                      type: 'image/jpg',
                      data: RNFetchBlob.wrap(result.upload_uri)
                    }
                  ]);
                } else {
                  Utils.showErrorAlert(errorMsg);
                }
              });
            }}
          >
            <AppText text={'Open image picker'} />
          </TouchableEffect>
          <TouchableEffect
            primary
            block
            large
            onPress={() => {
              Utils.selectVideo(({ success, response, errorMsg }) => {
                if (success) {
                  Logger.log('Video uri: ', response.uri);
                } else {
                  Utils.showErrorAlert(errorMsg);
                }
              });
            }}
            style={{ marginTop: 10 }}
          >
            <AppText text={'Open video picker'} />
          </TouchableEffect>
        </Content>
      </Container>
    );
  }
}
export default TabC;
