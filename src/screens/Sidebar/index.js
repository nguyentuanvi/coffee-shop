// @flow
import React, { Component } from 'react';
import { ImageBackground, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { StackActions, NavigationActions } from 'react-navigation';
import { ListItem, Thumbnail, View } from 'native-base';

import { Icon } from 'react-native-elements';

import { Grid, Col } from 'react-native-easy-grid';

import AbstractComponent from '../../components/AbstractComponent';
import Container from '../../components/Container';
import Content from '../../components/Content';
import { I18n } from '../../common';
import { logout } from '../../actions/loginAction';

import { sidebarBg, sanket } from '../../../assets';

import AppText from '../../components/AppText';

import styles from './styles';
const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })]
});
class SideBar extends AbstractComponent {
  render() {
    const navigation = this.props.navigation;
    return (
      <ImageBackground source={sidebarBg} style={styles.background}>
        <Content style={styles.drawerContent} contentContainerStyle={styles.contentContainer}>
          <ListItem
            button
            onPress={() => {
              navigation.navigate('TabA');
            }}
            iconLeft
            style={styles.links}
          >
            <Icon name="home" />
            <AppText style={styles.linkText} text={I18n.t('sidebar.home')} />
          </ListItem>
          <ListItem
            button
            onPress={() => {
              navigation.navigate('Calendars');
            }}
            iconLeft
            style={styles.links}
          >
            <Icon name="calendar" type="material-community" />
            <AppText style={styles.linkText} text={I18n.t('sidebar.calendars')} />
          </ListItem>
          <ListItem
            button
            onPress={() => {
              navigation.navigate('CalendarList');
            }}
            iconLeft
            style={styles.links}
          >
            <Icon name="calendar-multiple" type="material-community" />
            <AppText style={styles.linkText} text={I18n.t('sidebar.calendarList')} />
          </ListItem>
          <ListItem
            button
            onPress={() => {
              navigation.navigate('Agenda');
            }}
            iconLeft
            style={styles.links}
          >
            <Icon name="view-agenda" />
            <AppText style={styles.linkText} text={I18n.t('sidebar.agenda')} />
          </ListItem>
          <ListItem
            button
            onPress={() => {
              navigation.navigate('Share');
            }}
            iconLeft
            style={styles.links}
          >
            <Icon name="share" />
            <AppText style={styles.linkText} text={I18n.t('sidebar.share')} />
          </ListItem>
          <ListItem
            button
            onPress={() => {
              navigation.navigate('TForm');
            }}
            iconLeft
            style={styles.links}
          >
            <Icon name="input" />
            <AppText style={styles.linkText} text={I18n.t('sidebar.tform')} />
          </ListItem>
          <ListItem
            button
            onPress={() => {
              navigation.navigate('Chat');
            }}
            iconLeft
            style={styles.links}
          >
            <Icon name="chat" />
            <AppText style={styles.linkText} text={I18n.t('sidebar.chat')} />
          </ListItem>
          <ListItem
            button
            onPress={() => {
              navigation.navigate('Collapsible');
            }}
            iconLeft
            style={styles.links}
          >
            <Icon name="arrow-collapse" type="material-community" />
            <AppText style={styles.linkText} text={I18n.t('sidebar.collapsible')} />
          </ListItem>
          <ListItem
            button
            onPress={() => {
              navigation.navigate('Image');
            }}
            iconLeft
            style={styles.links}
          >
            <Icon name="image" />
            <AppText style={styles.linkText} text={I18n.t('sidebar.image')} />
          </ListItem>
          <ListItem
            button
            onPress={() => {
              navigation.navigate('Maps');
            }}
            iconLeft
            style={styles.links}
          >
            <Icon name="google-maps" type="material-community" />
            <AppText style={styles.linkText} text={I18n.t('sidebar.maps')} />
          </ListItem>
          <ListItem
            button
            onPress={() => {
              navigation.navigate('CreditCard');
            }}
            iconLeft
            style={styles.links}
          >
            <Icon name="credit-card" type="material-community" />
            <AppText style={styles.linkText} text={I18n.t('sidebar.credit')} />
          </ListItem>
        </Content>
        <View style={styles.logoutContainer}>
          <View style={styles.logoutbtn} foregroundColor={'white'}>
            <Grid>
              <Col>
                <TouchableOpacity
                  onPress={() => {
                    this.props.dispatch(logout());
                    navigation.dispatch(resetAction);
                  }}
                  style={{
                    alignSelf: 'flex-start',
                    backgroundColor: 'transparent'
                  }}
                >
                  <AppText style={[styles.linkText, styles.title]} text={'LOG OUT'} />
                  <AppText style={[styles.linkText, styles.title]} text={'Kumar Sanket'} />
                </TouchableOpacity>
              </Col>
              <Col>
                <TouchableOpacity
                  style={{ alignSelf: 'flex-end' }}
                  onPress={() => {
                    navigation.navigate('Profile');
                  }}
                >
                  <Thumbnail source={sanket} style={styles.profilePic} />
                </TouchableOpacity>
              </Col>
            </Grid>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

function mapStateToProps(state) {
  return {
    loginReducer: state.loginReducer
  };
}

SideBar = connect(mapStateToProps)(SideBar);

export default SideBar;
