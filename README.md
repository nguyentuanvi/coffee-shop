## React Native Arilliance Base Project

#This project is open for building components, libraries..

- Anyone has any ideas, please build and make pull request to master.

- No coding at master branch. Must create pull request for root users verifying.

#Built-in library

This project contains Authentication(Facebook, Google, Phonenumber) from react-native-firebase:

- [Firebase Authentication](https://rnfirebase.io/docs/v4.2.x/auth/getting-started)

Crashlytics from react-native-firebase:

- [Firebase crashlytics](https://rnfirebase.io/docs/v4.2.x/crashlytics/reference/crashlytics)

And some others library from firebase(firestore, database realtime, storage, notification)

#Structure in project

- Project using combination of redux, redux-sagas, api-sauce to handle API request and side effects. Please read carefully and follow the structure and flow.
- src/actions folder contains all actions in project. For each type of action, create a new file (such as auth action, [app_logic] action)
- src/reducers is structured similar to src/actions, contains all reducers in project.
- src/services folder contains all API requests and handler.
- src/resources folder contains all resources in project. Such as locales for multi languages. Highly recommended to use I18n module(is currently installed in project) for all text in project even if project not support multi languages.
- src/screens folder contains all screens(containers) in project. Only screens(containers) should connect to redux.
- src/components folder contains all common components using accross the project(such as AppHeader, Loading, Button, Input, ProgressBar,...). Make them easily to reuse and change style.
- src/common folder contains utilities, logger, app style, constants... for project. All constants should put in constants file. NO USING in components/screens level(ex: async storage key).
- src/boot folder contains redux store configs and first app screen.

#Notes for project
- HIGHLY RECOMMENDED NOT TO USE INLINE STYLE.
- ALWAYS USE COMMON COMPONENTS.
- USING LOGGER MODULE TO LOG INSTEAD.
- New Screen should extends AbstractComponent in order to increase performance.
- All static text must use I18n module.
- All colors, dimension, constants... store in files and always using these files to get values, DO NOT USE INLINE CONSTANTS.
- Notice duplication of code.
- NO MASSIVE COMPONENTS/SCREENS. Breakdown the screens/components into smaller pieces (good for performance/code maintain).
- Follow eslint rules.

Happy coding!

